# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=ksysguard
pkgver=5.12.8
pkgrel=0
pkgdesc="KDE system monitor utility"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	ki18n-dev kiconthemes-dev kinit-dev kitemviews-dev knewstuff-dev
	knotifications-dev kwindowsystem-dev libksysguard-dev
	cmake extra-cmake-modules kdoctools-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/ksysguard-$pkgver.tar.xz
	ksysguard-5.6.5-rindex-header.patch
	0001-Linux-softraid-define-_GNU_SOURCE-for-pipe2.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9ea21ba61866f1b36f15f41011dfcab562ede45cc3403f030184680f6e3b0717fc5ac774089784a3d14015728c3b721887083154b32b786e804dfc1aace89585  ksysguard-5.12.8.tar.xz
13a26451c459cff1d3b00af27c953d10c55e7e8f43ef7a6f0f54dd05dcb612546545c0170089e9499bb041f004cdacf19cb112d247a8ccf8fd4e77fea0d1c8bc  ksysguard-5.6.5-rindex-header.patch
0a424bb5e23f283d0cccfe86c0a6c2915f563a82e27a1dc16be8b4dc4b3d90ce116c4eb448dd1d5dc07602225468880206a5eb70c1b66d1e19e9a405fae7aa88  0001-Linux-softraid-define-_GNU_SOURCE-for-pipe2.patch"
