# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=atril
pkgver=1.22.2
pkgrel=0
pkgdesc="Document viewer for the MATE desktop environment"
url="https://mate-desktop.org"
arch="all"
options="!check" # testsuite requires X and py3-dogtail
license="GPL-2.0+ AND Afmparse AND Info-ZIP AND libtiff AND LGPL-2.0+ AND MIT AND LGPL-2.1+"
depends=""
makedepends="caja-dev djvulibre-dev gobject-introspection-dev gtk+3.0-dev
	intltool itstool libgxps-dev libsecret-dev libsm-dev libspectre-dev
	libxml2-dev libxml2-utils poppler-dev python3 tiff-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://pub.mate-desktop.org/releases/1.22/atril-$pkgver.tar.xz
	CVE-2019-11459.patch"

# secfixes:
#   1.22.1-r1:
#     - CVE-2019-1010006
#   1.22.1-r2:
#     - CVE-2019-11459

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-introspection \
		--enable-pixbuf \
		--enable-comics \
		--enable-xps
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="99ff55f84649dfb8de931ff2506ff0339852fbb7ed368cee1f6632ba243d2b0384cd0bd649d16c30317fbf786612f54c2404da43d14141e6f9c0944e64c34653  atril-1.22.2.tar.xz
ba4ec4b0e10d87f44f189a16cfe2419906e3776edc9bc14f7da9356a8953683e3f7efc441691df131497b08b892d3b291aab416310f259ee6bc0706cc4f02880  CVE-2019-11459.patch"
