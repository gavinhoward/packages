# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdegames
pkgver=19.08.2
pkgrel=0
pkgdesc="Library for common routines shared between KDE games"
url="https://www.kde.org/applications/games/"
arch="all"
options="!check"  # Tests require X11
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kcompletion-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtsvg-dev
	libsndfile-dev openal-soft-dev
	karchive-dev kbookmarks-dev kcodecs-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kdeclarative-dev kdnssd-dev
	kglobalaccel-dev kguiaddons-dev ki18n-dev kiconthemes-dev kio-dev
	kitemviews-dev kjobwidgets-dev knewstuff-dev kservice-dev kxmlgui-dev
	ktextwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-lang $pkgname-carddecks::noarch"
source="https://download.kde.org/stable/applications/$pkgver/src/libkdegames-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	# causes crashes in KPat:
	# https://bugs.kde.org/show_bug.cgi?id=402220
	# Qt doesn't care:
	# https://bugreports.qt.io/browse/QTBUG-72779
	cd "$pkgdir"/usr/share/carddecks
	local i; for i in ancient-egyptians future jolly-royal \
		konqi-modern standard xskat-french
	do
		rm -r "svg-$i"
	done
}

carddecks() {
	pkgdesc="Card decks for KDE card games"
	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/carddecks "$subpkgdir"/usr/share/
}

sha512sums="68ffcd6696b903c0d701996ead9f05ea2d35d72871840cb11ed05d9034bd2cc89017e5ef8fb8508452446075ed157dc6cbcda32762ed5d79ff35bf9e070c7b67  libkdegames-19.08.2.tar.xz"
