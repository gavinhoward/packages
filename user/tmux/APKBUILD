# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=tmux
pkgver=2.9a
pkgrel=1
pkgdesc="Tool to control multiple terminals from a single terminal"
url="https://tmux.github.io/"
arch="all"
license="MIT"
depends="ncurses-terminfo"
makedepends="bsd-compat-headers libevent-dev libutempter-dev ncurses-dev"
subpackages="$pkgname-doc"
source="https://github.com/tmux/tmux/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
	xterm-DECLRMM.patch"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm644 example_tmux.conf \
		"$pkgdir"/usr/share/doc/$pkgname/examples/$pkgname.conf
	for file in CHANGES README TODO; do
		install -m644 "$file" "$pkgdir"/usr/share/doc/$pkgname/
	done
}

sha512sums="aca6882688727c10c5647443fdd18bbd6c0f80b7a3bf9667903d1b89d523e604cd715f176f33f2e5673258f00e626a6dc273f80fe97ae4f91621814d89985713  tmux-2.9a.tar.gz
7fb006f8f24d60614fcaeadace933c2ad6674d2476980736a830f63a40d7110638942478322336849d47707c99ab7de0cb01ff39806a4c9144650daf2f666773  xterm-DECLRMM.patch"
