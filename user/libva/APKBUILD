# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libva
pkgver=2.5.0
pkgrel=0
pkgdesc="Video Acceleration (VA) API for Linux"
url="https://github.com/intel/libva"
arch="all"
options="!check"  # No test suite.
license="MIT"
depends=""
depends_dev="mesa-dev"
makedepends="$depends_dev autoconf automake libtool cmd:which"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/intel/$pkgname/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-x11 \
		--disable-wayland \
		--disable-static \
		--enable-shared
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ce810faa216fc9b3f466565b598018b20b8590692ec6b88874d2920bce8c5e875e429957e370ebba95aa2375f80c1c0f0d68cb6eb2276494c74b9da2a5547f96  libva-2.5.0.tar.gz"
