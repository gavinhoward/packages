# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=fortune-mod
pkgver=2.10.0
pkgrel=0
pkgdesc="Display random messages or quotations"
url="https://www.shlomifish.org/humour/fortunes/"
arch="all"
options="!check"  # Fails due to spaces in CMake files...
license="BSD-4-Clause"
depends=""
checkdepends="perl-file-find-object perl-io-all perl-test-differences
	perl-test-runvalgrind valgrind"
makedepends="cmake recode-dev"
subpackages="$pkgname-doc"
source="https://github.com/shlomif/fortune-mod/archive/$pkgname-$pkgver.tar.gz"
builddir="$srcdir/fortune-mod-fortune-mod-$pkgver/$pkgname"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DNO_OFFENSIVE=True \
		-DLOCALDIR=/usr/share/games/fortunes \
		${CMAKE_CROSSOPTS} \
		-Bbuild \
		.
	make -C build
}

check() {
	make -C build check
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="f5776d3afc52859b8959b0a51fc0dd57665b7d35b046958e6a08f29f9ad7915957ec4fec6b38d08a797a05163cc20a47e62a4e3d65034084e2003b8a69cc90e1  fortune-mod-2.10.0.tar.gz"
