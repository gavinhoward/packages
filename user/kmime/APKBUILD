# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmime
pkgver=19.08.2
pkgrel=0
pkgdesc="KDE support library for MIME"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 kcodecs-dev
	ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kmime-$pkgver.tar.xz
	egregious-versions.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# https://bugs.kde.org/show_bug.cgi?id=385479
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(headertest|messagetest|dateformattertest)'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2c7b4332197c7fec915cedde4faf2726abd5ca1c0ae9c57642b35807cd9e75f1ef549802a71a6fc22515ab914759f3b01bc88ff964591aceb3c50b39de7b6157  kmime-19.08.2.tar.xz
ae8e5836cefbf19f2aa5c775e3189a24b558840799c93fefc4d3863567d152bd14a2597241fcf0967d134cefedcf2c258f75f3c3bda4ec2cd2623f81a9c37c9f  egregious-versions.patch"
