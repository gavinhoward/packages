# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=claws-mail
pkgver=3.17.4
pkgrel=1
pkgdesc="User-friendly, lightweight, and fast email client"
url="https://www.claws-mail.org/"
arch="all"
license="GPL-3.0-only"
depends="compface"
makedepends="compface-dev curl-dev dbus-glib-dev enchant-dev gnutls-dev
	gpgme-dev gtk+2.0-dev libcanberra-gtk2 libcanberra-dev libetpan-dev
	libical-dev libnotify-dev librsvg-dev openldap-dev
	startup-notification-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://www.claws-mail.org/download.php?file=releases/claws-mail-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-perl-plugin \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4fc3b04d89c84b068654902d3d4f8ba66ec86c4ea9c4bd145fb3169dd26a2fcfc26adc8367b0ed90c69d095f6b1717ba3a9a52cc6d3e310a9dad1c3f733d8012  claws-mail-3.17.4.tar.xz"
