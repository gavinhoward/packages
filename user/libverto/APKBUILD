# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: 
pkgname=libverto
pkgver=0.3.1
pkgrel=1
pkgdesc="Main loop abstraction library"
url="https://github.com/npmccallum/libverto"
arch="all"
license="MIT"
makedepends="glib-dev libevent-dev libev-dev automake autoconf libtool"
subpackages="$pkgname-dev $pkgname-libev $pkgname-libevent $pkgname-glib"
source="$pkgname-$pkgver.tar.gz::https://github.com/npmccallum/$pkgname/archive/$pkgver.tar.gz
	dash.patch"

prepare() {
	default_prepare
	autoreconf -i
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-libev \
		--with-libevent
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

_mv_backend() {
	pkgdesc="$1 driver for libverto"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libverto-$1.so.* "$subpkgdir"/usr/lib/
}

libev() { _mv_backend libev; }
libevent() { _mv_backend libevent; }
glib() { _mv_backend glib; }

sha512sums="8b46a5c410be210e52b92dccfe6e677026b26fd6cab1ee7a97d7eab4d4951913bde8768a77942189a0188ee336d5dcca2a985e9edc3a8c7529f164a7e5077bd8  libverto-0.3.1.tar.gz
414242414a71c36744be07e034a75d3c95d7d41682eb8431115b25e2b09040939ee3aaaaee8c9ba303d4f5ab21ae39de2843b6e137659deba2bc0befda208a8f  dash.patch"
