# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kalgebra
pkgver=19.08.2
pkgrel=0
pkgdesc="Graph calculator and plotter"
url="https://www.kde.org/applications/education/kalgebra/"
arch="all"
license="GPL-2.0-only"
depends="kirigami2 qt5-qtquickcontrols"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev analitza-dev ncurses-dev ki18n-dev kdoctools-dev kio-dev
	kconfigwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kalgebra-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4cb1013dd20b806a7fcd629328662a8b1c6efa81c685d4bd6b1940a8142dd6fd7ac5ad46636743e26d17b37c283fa51f94203878ae1f255720da71a2e8810654  kalgebra-19.08.2.tar.xz"
