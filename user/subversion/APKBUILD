# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=subversion
pkgver=1.12.2
pkgrel=0
pkgdesc="Version control system from 2000"
url="https://subversion.apache.org/"
arch="all"
options="!check"  # Test suite no longer works with Python 3:
                  # https://issues.apache.org/jira/browse/SVN-4811
license="Apache-2.0"
depends=""
depends_dev="apr-dev apr-util-dev linux-headers"
makedepends="apr-dev apr-util-dev cyrus-sasl-dev db-dev dbus-dev file-dev
	kdelibs4support-dev kwallet-dev libsecret-dev lz4-dev openssl-dev
	serf-dev sqlite-dev utf8proc-dev zlib-dev autoconf automake python3
	perl-dev swig"
subpackages="$pkgname-dev $pkgname-doc $pkgname-gnome $pkgname-kwallet
	$pkgname-pl $pkgname-lang"
source="https://www-eu.apache.org/dist/subversion/subversion-$pkgver.tar.bz2
	python3-bang.patch
	"

# secfixes:
#   1.12.2-r0:
#     - CVE-2018-11782
#     - CVE-2019-0203

build() {
	# this is only needed for autogen.sh
	_PATH=$PATH
	ln -s /usr/bin/python3 "$srcdir"/python
	export PATH=$srcdir:$PATH
	./autogen.sh
	rm "$srcdir"/python
	export PATH=$_PATH
	# this is needed to fix the apr issue as well as the patch
	# https://issues.apache.org/jira/browse/SVN-4813
	export CPPFLAGS="$CPPFLAGS -P"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-kwallet \
		--with-swig
	make

	make swig-pl-lib
	(cd subversion/bindings/swig/perl/native; perl Makefile.PL)
	# need override LD_RUN_PATH with something valid, otherwise we get
	# empty rpath which is not a good idea.
	make -j1 -C subversion/bindings/swig/perl/native \
		LD_RUN_PATH="/usr/lib" EXTRALIBS="-lapr-1"
}

check() {
	make check
	make check-swig-pl
}

package() {
	make DESTDIR="$pkgdir" -j1 install install-swig-pl-lib

	make pure_vendor_install -C subversion/bindings/swig/perl/native \
		PERL_INSTALL_ROOT="$pkgdir"
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete
}

gnome() {
	pkgdesc="$pkgdesc (GNOME Keychain integration)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libsvn_auth_gnome* "$subpkgdir"/usr/lib/
}

kwallet() {
	pkgdesc="$pkgdesc (KDE Wallet integration)"
	install_if="$pkgname=$pkgver-r$pkgrel kwallet"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libsvn_auth_kwallet* "$subpkgdir"/usr/lib/
}

pl() {
	pkgdesc="$pkgdesc (Perl bindings)"
	install_if="$pkgname=$pkgver-r$pkgrel git"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*perl* "$subpkgdir"/usr/lib/
}

sha512sums="b1f859b460afa54598778d8633f648acb4fa46138f7d6f0c1451e3c6a1de71df859233cd9ac7f19f0f20d7237ed3988f0a38da7552ffa58391e19d957bc7c136  subversion-1.12.2.tar.bz2
1b96b791f70c2f6e05da8dbc9d42ccadf4603f25392c6676c4e30ecdb142ce74dd9b8dc27dc68b1cb461f4409d79c4c2aeed1d39a5a442d9349079a819358f5a  python3-bang.patch"
