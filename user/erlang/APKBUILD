# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=erlang
pkgver=22.1.1
pkgrel=0
pkgdesc="Soft real-time system programming language"
url="https://www.erlang.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="autoconf automake flex libxml2-utils libxslt-dev m4 ncurses-dev
	openssl-dev perl unixodbc-dev"
subpackages="$pkgname-dev"
source="erlang-$pkgver.tar.gz::https://github.com/erlang/otp/archive/OTP-$pkgver.tar.gz
	fix-wx-linking.patch
	"
builddir="$srcdir/otp-OTP-$pkgver"

build() {
	./otp_build autoconf
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared-zlib \
		--enable-ssl=dynamic-ssl-lib \
		--enable-threads \
		--disable-hipe
	make
}

check() {
	local _header
	export ERL_TOP=$builddir

	make release_tests

	for _header in erl_fixed_size_int_types.h \
		${CHOST}/erl_int_sizes_config.h \
		erl_memory_trace_parser.h; do
		cp erts/include/$_header erts/emulator/beam/
	done
	cd release/tests/test_server
	$ERL_TOP/bin/erl -s ts install -s ts smoke_test batch -s init stop
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ee1a3bb355ffd083c3355caf60189799d322cff2a01314ba5df141ccdc4de45dbfe3967e661e3dfbcf14fc04dc1f855d7a3660f9b2daa2451a83422eda7cba39  erlang-22.1.1.tar.gz
91c62e6a894d6f1ae371025e49c02ff25f1cc244fb18c8eae87c9e031d8216bab901b8d278df67db08f260f0d56fa18122f191405e4d58ca64934259f156b907  fix-wx-linking.patch"
