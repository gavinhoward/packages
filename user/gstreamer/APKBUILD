# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gstreamer
pkgver=1.16.1
pkgrel=0
pkgdesc="GStreamer multimedia framework"
url="https://gstreamer.freedesktop.org/"
arch="all"
options="!check"
license="LGPL-2.0+"
depends=""
depends_dev="libxml2-dev"
makedepends="$depends_dev bison flex gobject-introspection-dev glib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools $pkgname-lang"
replaces="gstreamer1"
source="https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-$pkgver.tar.xz
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-introspection \
		--with-package-name="GStreamer (${DISTRO_NAME:-Adélie Linux})" \
		--with-package-origin="${DISTRO_URL:-https://www.adelielinux.org/}" \
		--disable-fatal-warnings \
		--with-ptp-helper-permissions=none
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Tools and files for GStreamer streaming media framework"
	# gst-feedback needs this
	depends="pkgconfig"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}
sha512sums="30abcf20fa16cdedbacbecabef66367e614c9c5865311238397afbbe30fe6588909ddd7e917c4ec0749c4095c6f73b4de2e57a93ff2c1b97629206b05fc06173  gstreamer-1.16.1.tar.xz"
