# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gsl
pkgver=2.5
pkgrel=1
pkgdesc="Modern numerical library for C and C++"
url="https://www.gnu.org/software/gsl/gsl.html"
arch="all"
license="GPL-3.0+"
depends=""
makedepends=""
subpackages="$pkgname-dev $pkgname-doc"
source="https://ftp.gnu.org/gnu/gsl/gsl-$pkgver.tar.gz
	dont-disable-deprecated.patch
	aarch64-test-failure.patch
	gsl-2.4-portable.patch
	"

# dont-disable-deprecated.patch is workaround for:
# https://github.com/SciRuby/rb-gsl/issues/40

build() {
	cd "$builddir"
	[ $CTARGET_ARCH != "ppc" ] || export ac_cv_c_ieee_interface=unknown
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="5b4c5c023f9029ce220f4e09aa4d0234fed94d42ac224d58fda095fe0532d54237a7c33278f8b5d0ba051f6004486edb38d0adb4fcb49337a8c1d8a18cf4a24a  gsl-2.5.tar.gz
88d40e599a9e619d8968f9848a91c54492d99032734371ee23072c8dae9d9920da445c1f8a880baa613479facec4afca3d3dec1070c240e5dfd5a662a41c92e8  dont-disable-deprecated.patch
68b685270a377341b3c3ce566ae6eff4ebfc27b75a73f3c7915c57446798bdcca7c1d9f0fa4ce8a50118b371bfe3e2947f9bf33590c86e85db8e807b3b0deae6  aarch64-test-failure.patch
f6a22e82c8f4a1d5ea7b647cbd015cdb9d2e4e9d39728e8f2dba3fdefac64d3f7c8fbc5ec597e6ab315ec1bd49e0e3af4aba091b358a68d9d789884331f4be24  gsl-2.4-portable.patch"
