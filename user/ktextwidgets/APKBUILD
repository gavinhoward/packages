# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ktextwidgets
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework providing widgets for text manipulation"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kcompletion-dev kconfigwidgets-dev kiconthemes-dev
	kservice-dev sonnet-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktextwidgets-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="0a3e12a422bb8aa1c79cf96cbd95d7544ba812e12b9a9b838efc014336e56c3f21bce80084ec9ed3c164b317dd9588db3ef28a729b01bdd05e9ea024d47ce767  ktextwidgets-5.54.0.tar.xz"
