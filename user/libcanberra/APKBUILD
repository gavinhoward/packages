# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libcanberra
pkgver=0.30
pkgrel=4
pkgdesc="Event sound library for free desktops"
url="http://0pointer.de/lennart/projects/libcanberra/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends="sound-theme-freedesktop"
makedepends="alsa-lib-dev gstreamer-dev gtk+2.0-dev gtk+3.0-dev libogg-dev
	libtool libvorbis-dev lynx pulseaudio-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-gtk2 $pkgname-gtk3
	$pkgname-gstreamer $pkgname-pulse"
source="http://0pointer.de/lennart/projects/$pkgname/$pkgname-$pkgver.tar.xz"

prepare() {
	cd "$builddir"
	update_config_sub
	default_prepare
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-oss
	make
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

gtk2() {
	pkgdesc="Gtk+ 2.x Bindings for libcanberra"
	install_if="$pkgname=$pkgver-r$pkgrel gtk+2.0"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libcanberra-gtk.so.* \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/gtk-2.0 \
		"$subpkgdir"/usr/lib/
}

gtk3() {
	pkgdesc="Gtk+ 3.x Bindings for libcanberra"
	install_if="$pkgname=$pkgver-r$pkgrel gtk+3.0"
	mkdir -p "$subpkgdir"/usr/lib/gnome-settings-daemon-3.0/gtk-modules \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/share/gnome/autostart \
		"$subpkgdir"/usr/share/gnome/shutdown \
		"$subpkgdir"/usr/share/gdm/autostart/LoginWindow
	mv "$pkgdir"/usr/lib/gtk-3.0 \
		"$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libcanberra-gtk3.so.* \
		"$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/bin/canberra-gtk-play \
		"$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share/gnome/autostart/libcanberra-login-sound.desktop \
		"$subpkgdir"/usr/share/gnome/autostart
	mv "$pkgdir"/usr/share/gnome/shutdown/libcanberra-logout-sound.sh \
		"$subpkgdir"/usr/share/gnome/autostart/
	mv "$pkgdir"/usr/share/gdm/autostart/LoginWindow/libcanberra-ready-sound.desktop \
		"$subpkgdir"/usr/share/gdm/autostart/LoginWindow/
	mv "$pkgdir"/usr/lib/gnome-settings-daemon-3.0/gtk-modules/canberra-gtk-module.desktop \
		"$subpkgdir"/usr/lib/gnome-settings-daemon-3.0/gtk-modules/
}

gstreamer() {
	pkgdesc="GStreamer backend for libcanberra"
	install_if="$pkgname=$pkgver-$pkgrel gstreamer"
	mkdir -p "$subpkgdir"/usr/lib/libcanberra-$pkgver
	mv "$pkgdir"/usr/lib/libcanberra-$pkgver/libcanberra-gstreamer.so \
		"$subpkgdir"/usr/lib/libcanberra-$pkgver/
}

pulse() {
	pkgdesc="PulseAudio backend for libcanberra"
	install_if="$pkgname=$pkgver-$pkgrel pulseaudio"
	mkdir -p "$subpkgdir"/usr/lib/libcanberra-$pkgver
	mv "$pkgdir"/usr/lib/libcanberra-$pkgver/libcanberra-pulse.so \
		"$subpkgdir"/usr/lib/libcanberra-$pkgver/
}

sha512sums="f7543582122256826cd01d0f5673e1e58d979941a93906400182305463d6166855cb51f35c56d807a56dc20b7a64f7ce4391368d24990c1b70782a7d0b4429c2  libcanberra-0.30.tar.xz"
