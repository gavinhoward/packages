# Contributor: Gentoo Rust Maintainers <rust@gentoo.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=ripgrep
pkgver=11.0.2
pkgrel=0
pkgdesc="Recursively searches directories for a regex pattern"
url="https://github.com/BurntSushi/ripgrep"
arch="all"
license="Unlicense"
makedepends="cargo pcre2-dev"
subpackages="$pkgname-zsh-completion:_zshcomp:noarch"
source=""

# dependencies taken from Cargo.lock
cargo_deps="
$pkgname-$pkgver
aho-corasick-0.7.4
atty-0.2.13
base64-0.10.1
bitflags-1.1.0
bstr-0.2.6
bytecount-0.5.1
byteorder-1.3.2
cc-1.0.38
cfg-if-0.1.9
clap-2.33.0
crossbeam-channel-0.3.9
crossbeam-utils-0.6.6
encoding_rs-0.8.17
encoding_rs_io-0.1.6
fnv-1.0.6
fs_extra-1.1.0
globset-0.4.4
grep-0.2.4
grep-cli-0.1.3
grep-matcher-0.1.3
grep-pcre2-0.1.3
grep-printer-0.1.3
grep-regex-0.1.4
grep-searcher-0.1.5
ignore-0.4.9
itoa-0.4.4
jemalloc-sys-0.3.2
jemallocator-0.3.2
lazy_static-1.3.0
libc-0.2.60
log-0.4.8
memchr-2.2.1
memmap-0.7.0
num_cpus-1.10.1
packed_simd-0.3.3
pcre2-0.2.1
pcre2-sys-0.2.2
pkg-config-0.3.15
proc-macro2-0.4.30
quote-0.6.13
regex-1.2.0
regex-automata-0.1.8
regex-syntax-0.6.10
ryu-1.0.0
same-file-1.0.5
serde-1.0.98
serde_derive-1.0.98
serde_json-1.0.40
strsim-0.8.0
syn-0.15.42
termcolor-1.0.5
textwrap-0.11.0
thread_local-0.3.6
ucd-util-0.1.5
unicode-width-0.1.5
unicode-xid-0.1.0
utf8-ranges-1.0.3
walkdir-2.2.9
winapi-0.3.7
winapi-i686-pc-windows-gnu-0.4.0
winapi-util-0.1.2
winapi-x86_64-pc-windows-gnu-0.4.0
wincolor-1.0.1
"

source="$source $(echo $cargo_deps | sed -E 's#([[:graph:]]+)-([0-9.]+(-(alpha|beta|rc)[0-9.]+)?)#&.tar.gz::https://crates.io/api/v1/crates/\1/\2/download#g')"

prepare() {
	export CARGO_HOME="$srcdir/cargo-home"
	export CARGO_VENDOR="$CARGO_HOME/adelie"

	(builddir=$srcdir; default_prepare)

	mkdir -p "$CARGO_VENDOR"
	cat <<- EOF > "$CARGO_HOME/config"
		[source.adelie]
		directory = "${CARGO_VENDOR}"

		[source.crates-io]
		replace-with = "adelie"
		local-registry = "/nonexistant"
	EOF

	for _dep in $cargo_deps; do
		ln -s "$srcdir/$_dep" "$CARGO_VENDOR/$_dep"
		_sum=$(sha256sum "$srcdir/$_dep.tar.gz" | cut -d' ' -f1)
		cat <<- EOF > "$CARGO_VENDOR/$_dep/.cargo-checksum.json"
		{
			"package":"$_sum",
			"files":{}
		}
		EOF
	done
}

build() {
	export CARGO_HOME="$srcdir/cargo-home"
	export PCRE2_SYS_STATIC=0
	cargo build -j $JOBS --features pcre2 --release
}

check() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo test -j $JOBS --features pcre2 --release
}

package() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo install --features pcre2 --path . --root="$pkgdir"/usr
	rm "$pkgdir"/usr/.crates.toml
}

_zshcomp() {
	pkgdesc="ZSH completion for ripgrep"
	license="BSD-3-Clause"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm0755 $builddir/complete/_rg "$subpkgdir"/usr/share/zsh/site-functions/_rg
}

sha512sums="8a9e75ad442bc12a6b189eca39b09e890a2bbf6d9b3b55515517c97201bea8577851da443a108fe7524ec4bbe35ea624aca9c872ab31d991baab282e7265383c  ripgrep-11.0.2.tar.gz
06863d85a3183c7ace014d6a8345d47147df53475c194030607314b80e00890625ae88e3e669d15e54704f2d3132dd6bf56dc057b87dab7f06c5deaf50d29b79  aho-corasick-0.7.4.tar.gz
4554ca7dedb4c2e8693e5847ef1fe66161ed4cb2c19156bb03f41ce7e7ea21838369dabaf447a60d1468de8bfbb7087438c12934c4569dde63df074f168569ad  atty-0.2.13.tar.gz
674a8cbee95a41f755499a4e3bb93ebd3f80140c3e8e2d44a0b73890ee423754e7ba8abcd92132683cd54501ff5d14438d023a202eaf12776aecbe36df9e5535  base64-0.10.1.tar.gz
e3e611cf35a1ed4930727d530e6c78add895bd96636ca1354f1269b3d0e36e77fbb9ec850fe1f448a10f09ea2b2f89c2b16bb96b7da585851ce4c29a308968e3  bitflags-1.1.0.tar.gz
e5a4dd8061208bd153052fc991d827e96f5d3358ffde84c68d4fcee4ef5ec65de4482795f4d4fcdd5b2b4cea4b69a7e35e369f09a476ab536ecec54aa8d051d4  bstr-0.2.6.tar.gz
9017ed0545266c1f55cc9cd65fb63906bc50ea28c8de30770cb618029a605744a5234487766d52c77ded01a8b08c91d08264175eb4f4352e6777ad5d152670a9  bytecount-0.5.1.tar.gz
2ef6b986926a4671dd945583730b0bfd4bd5e75b62a8a70c2875328157ba95f2c1b17c534d905e9b287457bd34363c1a33fd3dee9217c371032393ebbe206a8f  byteorder-1.3.2.tar.gz
0d08ddd3f30c52eaca8195f9cd473ce3fdde6c6788481a33a8970c842781ce1eeb521023291f75d7c03279f524e8739abf3cc66f51ebf6e68945626589f37d30  cc-1.0.38.tar.gz
45f7322217d291b3905ffdc45cadd5a7a7baf440f9a82a5b5596192ed0ac54353a3ecae0326d5807aae99bc4d79e0406d71bd65745ec8d9f8815a7c9436d648c  cfg-if-0.1.9.tar.gz
f1075031414d48e4340bfe308904a95a31b72460724773c52a0bc8c004e625a04c904a39fc5420cb8c26a633321f9b5f4f69019c7aae5ed89900b63ed8c21a91  clap-2.33.0.tar.gz
4cc876bab2bd8874cee4b96bc490e77778f10e99ab624ed7a8b73be94b59a40bcb340fdb81a1d14242f6a795557c9f8bcdcf17d6bf6829aff85c7c1e8bf00919  crossbeam-channel-0.3.9.tar.gz
771ac1a55a043600e7d8b720f9b5a32da2ec09efaa4dec4202f7b78a505eb391a842b7abc91516ccd287c3207c634b342b0bc8ebb3c0c7a0d73b7fb29badc9f5  crossbeam-utils-0.6.6.tar.gz
db7a7d469d34feaa899195c336fb9211ee358c226597d0900850fbafac2ee39a89bff253663744ec737a055bf7582e96b9825e96e679da4bb9fb3b0cc59d1453  encoding_rs-0.8.17.tar.gz
ef8e9d93ff5395c0a5dc0f777907632cd2d38cdef4d0b9d4ee0697701a1e4320221605ff699d6c66c0d80c582378ad7ca10a56c3914e8466192189a47c3cd9f9  encoding_rs_io-0.1.6.tar.gz
f1356b3da25f76cb3ccb4336ff54d4967f1dc7523eae6cba21a4349b8ce563516f6a2aa10d626cd5bb6046b55ac2f246e61e4e526a03fad5e78d0ea174841844  fnv-1.0.6.tar.gz
c001a37a23a614f1752a45fbf392694911c8d06bb5af8b8a40f2dcba9c80c67ab634de0dd4fa951007072204b162430fed2c89bbb11ac2fca62dfa5cd4cbd46a  fs_extra-1.1.0.tar.gz
b9da7f4d67d4269b0b3564d65904c9cca0db7df6d33255f6022eee614f0ad03f3dc6f9e52e52fca7461308d9f01dac1f3588baf87aef735c266561b1119f9985  globset-0.4.4.tar.gz
c6983df9dcc268bc9835173189afc28ffec9dd234a0949389a40629e5644707b7c1b90025882c2580cc5f963fc4902070b191ecc6cacd602fa4c140ee8bdfbd1  grep-0.2.4.tar.gz
5867afd60939e24b7baa848f72342fc7cf927f198b7c945a00a93c8c504f18fdc75adbb1f28ad559dd7bb301ace6bd2641a79d138c5f742c366d9cd7a3452da9  grep-cli-0.1.3.tar.gz
74a9786be82cbda9f446aef20752afcf7796d91eac5a6b12060fe844ccd676c5f9097332828c267690eaf814356ebcb4dd2f4a674e32e57d39c3914c9ca399ec  grep-matcher-0.1.3.tar.gz
49777885abbf028ac7845d76faf4d1f566d7551e71cdd9788bcc237f110d1f77fb438c42e965ec0b3b2943022acf0fd1b28f7b6e5ed3bf20525476eb9e9aa749  grep-pcre2-0.1.3.tar.gz
99ee04909054b1a63890de3711c6c09e8ba206a9a30a2f61e2f8b82a23f8b8711cf00102d5aaae2b6b965b6a7c01e09380254841e94fa7d3dbaf63f5c03a854e  grep-printer-0.1.3.tar.gz
7756cb4e448246186c49a0246084d634f9194259899efe3eb078a09917e350da7e37d5e03187c55ab5da23e265b4912cdfceb8bd23fe27bbbc2e5a9cda562cc0  grep-regex-0.1.4.tar.gz
ac09b3e7dacfd2ad3587c23fee858d9c726b21e638ddb6f9c4b992649911400e3ec44c2b90d13fb8212ae38d3a8ff38220cc922af76c1291b8fa2e9dd098fb16  grep-searcher-0.1.5.tar.gz
e1ab4c2708a1301156b9da6f084c9b8cc7e9a2c8a657043cc6c47d4fe235d5241761c4c1bbb9bb9e65655134fea1320398d67855bfdd595c87d94ee90c480b38  ignore-0.4.9.tar.gz
f5e04bd908457e7592243ce64a99c5283428b767f4cc17d77946770411b06fccb0250625263c3e84a02a018ea7e8a0e4216e1929a71988bab8e1dbf603d3801d  itoa-0.4.4.tar.gz
b718dd98474d16fa5448bba62ac5e18a9e9798f31fce19dbeb4a12526c63e78f306454e0d9e2c6c5a3fd95660bfe82bb5fb09e4091cd8b43706eca2c4872b647  jemalloc-sys-0.3.2.tar.gz
e3fd9d377d6465519e73a91d5639d47bfd6cdf5898d551c49e3efd3269d14a75bb9163980e13ff429e7dc637b8430cdfdb1f5d6ec52993fcf7b6247a69850ac6  jemallocator-0.3.2.tar.gz
08288790139876765b6d4a5988f47fd2a4bfc77c2f2406ad44e64920a471b5655c7f54cb197e5a40c29ee8b42aecbbefaac2b6f4a7dd2b5e24dd92c46cb9b822  lazy_static-1.3.0.tar.gz
649bb508f5e2800712618e76686c12dd3e7956ce2197d620b1b53427496296889f2743cf060e53400b33ea86982dff2c7d7c85b151fc1233895fb4c7f786b39c  libc-0.2.60.tar.gz
0b71f97d5964134b5eea1332347e177806b2f171d0be5c410c0ff1539470b242ba9f0933fafd853e4171a43b5e373a150af18918924be431c7216022553a8a3b  log-0.4.8.tar.gz
0f5e11507a4235edaa8ef639753abf62f1c187c8c6e35b0c80c9306958e41893492d4995e28bf28e5710e8739c7e75bb75a515507ec87870741a3b426ba8f44d  memchr-2.2.1.tar.gz
3a7c7f963111c2afeaa0381aaa6a57f0f57600392693ee1807d54771bc058ea0f86ac6e8afbe858e45f9f17b685430bf256dba1126b8074ace3aafc07bc14bfa  memmap-0.7.0.tar.gz
f1f7408dc1cfe7c718928d8e3e219e3001ce4207467a0a129546f2729ba43a7bf334cd5d328a7f8f8b1c276382c8a0f9d7abf60a3ae2c32f4adafa96c6ea62cd  num_cpus-1.10.1.tar.gz
f0198f5c589be082053ae156372ea826c66e31aad993f64af097faf36045f1d1b6cc4b4b46d9cc8a1c7a28e11f707026df6c419a42e2011b1c4a34f579018826  packed_simd-0.3.3.tar.gz
6e9a0d60c8bc633d4f19fbbb3934f86bd95e34dcfec8aad55dae5ebe4ed854dcb42ad265f4b0ed5c1a8683a032268bb38b2c3899d25428311cebf97789c62d0a  pcre2-0.2.1.tar.gz
cae12d44a43c05b2303f230112f8cce349141a697a0ed7ab8b9acc1a812f8ae8db156dfdcf40eae3b51c143b31cbf842dddcb133eb7e0430ef78cf36906ff652  pcre2-sys-0.2.2.tar.gz
645aed8e24ce99085e4f152eac511df50646f17705d57bd8566e70e17c18bd6b05001479c94b2425e45c67d4f84797784053a8299957a1492289853755a7867d  pkg-config-0.3.15.tar.gz
73a8de3f1d76a8baf2d45afc1497bba8c0cbf231bf9b6750b9cee2473f492d5f3957ac149998da720acc8287c96d604971b51dcdfa629523bbdd97c297856ac0  proc-macro2-0.4.30.tar.gz
bafa9ba42ea6ff2d6df652384485c58327de6eaea2832423eedd8ef8b4aace673c23b70f1f22106515ac13d7f625cb8b1a5e8c4388c1701ea3cd86fb9ac3056e  quote-0.6.13.tar.gz
ed4317e4b91b91b4807daa56d8070a5cba5f99ecbaa23b78b92438d19415f38dc1befb11e8b5ade63cd1b392fb83778350583cf531c374303c6f3405e6b4784d  regex-1.2.0.tar.gz
41ad4ad05b0147e663f63e4bdab8281d3dc15f76663bb9eb3d9f6a230350b71e7f3f42bd18e6628a7cd971655a6b13aea2308d7f124bccca24d01ea9ed15d61a  regex-automata-0.1.8.tar.gz
30b5cc9887210c5594d57be7f05e25475b56732c9a0be02452aacf80b456f12450b63f4acbb67badb3ad16f22d244431f5bcf5ee30b9ce8f35dcdeddf3972175  regex-syntax-0.6.10.tar.gz
b5dd360611fdf76ff13d377c1c79ce09a4fab90a8b7fc917a4424b8246c8a0da7d3ec515b8c69b695d936b1207072d173b9f40fd5cb218f1eab947862112caf0  ryu-1.0.0.tar.gz
33be985579210b64eed7e26f24a695e2a59992ae51723e5aa026ba34dbb05be4c377bb25db9797dca1f70aa4be58f33cae32a89b61fe6b9353de9fb902ba2dcc  same-file-1.0.5.tar.gz
a949d803c7c66312f280483c532c705f8b3a2b4d058b717195ffc539ce9e92bff707a585cb9e2f52c16152107edab40ff8bc4cf825c06f6b73f1fa189cb9dba7  serde-1.0.98.tar.gz
16313a0fade030fce860f02daffd47febfe3c470b5ecf3a8a53d88eae42b251fba56dab7caceb3560f562860957be4fc66ece06d176757c61b8497f5508a1ece  serde_derive-1.0.98.tar.gz
d09bc95c963f510686106d9885f3420b9eabba8bf32626597dafd43ffbe91ea72ee4a3fedfca922794a727214d73929970acced8eccaa23616cde33dfde9f842  serde_json-1.0.40.tar.gz
1d55a8d946cd55f5f37d06aea536549ded95739fa58c0f2da285a0041154c181f663682bdcac643aa198b3e762d694a04f058db985c62ebe22b5c16327ba6d34  strsim-0.8.0.tar.gz
b2feecfb9d05e2d2a184d92ae78f873147e4950641dfc9300988d6974a85a63690c41bf22ecede2a2ac8e45ffe41b13011e5eaa82be5ce6037b40b9b247bd989  syn-0.15.42.tar.gz
d729ef6da68be8e2ce19ef02336193d8baea97a28cc8789806587da829471222a0c7669b4485ef81b1d52e85096d787dcfd4f064c894904602b595aa024fcdca  termcolor-1.0.5.tar.gz
f5c0fe4f28ff1a3a0931e8e235b5157a45f67967985bcc752418c5ec3481fca44a8ae4800088889b37e8cd0533f53d3c456d5ffd19b767b3f83a87b49a2e209a  textwrap-0.11.0.tar.gz
cd783d3d9caec43868da1f6118d4c4d520e03b9f1049d8f15d2c12482989401d3aee748e04a149953d35e5d6487355c2891d44569ef688bc1d45f01b6461d253  thread_local-0.3.6.tar.gz
14de9b370a49fe6fcce871e223daa257ca5e50e3d8cdfa3c6800db89ec4a0e6bac55e2b73990768fbc2a13a16d77c8b59015c4c4fe413735e7c893ac3651f5b3  ucd-util-0.1.5.tar.gz
bd5ac5f0433953d79408074239edc7c43ce23d56659d467805d81ab01c576a3cf77ccedb3bba41d48bc4ad46a8905ac8a1927b99312053ef6295fd940a6766d2  unicode-width-0.1.5.tar.gz
cc5343e2166938322cfd7c73f1f918f2a9c46846ac0ef55933d1e44cdfaf6f7da2b7ff18b68e356c47b6d8ba5565eda0db42c347dcbde830683f341ac2b1849d  unicode-xid-0.1.0.tar.gz
24907ad7ae1a02713e6ecc62e0c73488abea338f0dd3b49291b914ca907b3a220cb90f8ca409c6aa57d2e0e5d8ca8c44cd310081ffe7be9208952d73ec53b9f8  utf8-ranges-1.0.3.tar.gz
43b8f629a43195c8cd8ad6821b431dd3648a19e6abb83f78deaa3300e7dafa32b31b3c89a228499585e3da4520ca26d82dabf938c1204c8011e5516b3b82da7b  walkdir-2.2.9.tar.gz
6871b93ad8d48e39b90cb7b31b3132f84665f965b4dfe06fcebdfb873e7d099007cf3d7a50e832a941c3425ad2f39c3ab48a77151e60863685b97fc05c71d134  winapi-0.3.7.tar.gz
a672ccefd0730a8166fef1d4e39f9034d9ae426a3f5e28d1f4169fa5c5790767693f281d890e7804773b34acdb0ae1febac33cde8c50c0044a5a6152c7209ec2  winapi-i686-pc-windows-gnu-0.4.0.tar.gz
54514420da9851f9657f888d8b198b3a97a6009b7e965d5a23ec471251f6548c2a58c716a5f48fb5d83a5775503d696da98eaed7b71fdd9ba7cd74ba6923b11a  winapi-util-0.1.2.tar.gz
4a654af6a5d649dc87e00497245096b35a2894ae66f155cb62389902c3b93ddcc5cf7d0d8b9dd97b291d2d80bc686af2298e80abef6ac69883f4a54e79712513  winapi-x86_64-pc-windows-gnu-0.4.0.tar.gz
f9914b8b416f3810f5199e85b8088c669bff3cbc0f5b86fdb5b600bbd0554465a559bae3ac918fb7197780663e94bf645ede8f35b60ab0e6a6cf2305b1eb99e1  wincolor-1.0.1.tar.gz"
