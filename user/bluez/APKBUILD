# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bluez
pkgver=5.51
pkgrel=0
pkgdesc="Linux Bluetooth protocol stack"
url="http://www.bluez.org/"
arch="all"
license="GPL-2.0+"
depends="consolekit2 dbus"
makedepends="alsa-lib-dev dbus-dev eudev-dev glib-dev libical-dev
	linux-headers"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-bccmd
	$pkgname-btmon $pkgname-cups $pkgname-deprecated $pkgname-hid2hci
	$pkgname-obexd $pkgname-openrc"
source="https://www.kernel.org/pub/linux/bluetooth/bluez-$pkgver.tar.xz
	bluetooth.initd
	rfcomm.initd
	rfcomm.confd
        001-bcm43xx-Add-bcm43xx-3wire-variant.patch
        002-bcm43xx-The-UART-speed-must-be-reset-after-the-firmw.patch
        003-Increase-firmware-load-timeout-to-30s.patch
        004-Move-the-43xx-firmware-into-lib-firmware.patch
	bluez-5.40-obexd_without_systemd-1.patch
	disable-lock-test.patch
	fix-endianness.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--libexecdir=/usr/lib \
		--disable-systemd \
		--disable-client \
		--enable-library \
		--enable-deprecated
	make
}

check() {
	make check
}

package() {
	make install DESTDIR="$pkgdir"
	install -D -m644 src/main.conf "$pkgdir"/etc/bluetooth/main.conf

	install -Dm755 "$srcdir"/bluetooth.initd "$pkgdir"/etc/init.d/bluetooth
	install -Dm755 "$srcdir"/rfcomm.initd "$pkgdir"/etc/init.d/rfcomm
	install -Dm644 "$srcdir"/rfcomm.confd "$pkgdir"/etc/conf.d/rfcomm
	install -Dm755 test/simple-agent "$pkgdir"/usr/bin/bluez-simple-agent
}

bccmd() {
	pkgdesc="Bluez utility for the CSR BCCMD interface"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/bccmd "$subpkgdir"/usr/bin/
}

btmon() {
	pkgdesc="Bluez bluetooth monitor"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/btmon "$subpkgdir"/usr/bin/
}

cups() {
	pkgdesc="Bluez backend for CUPS"
	depends="cups"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/cups "$subpkgdir"/usr/lib/
}

hid2hci() {
	pkgdesc="Put HID proxying bluetooth HCI's into HCI mode"
	replaces="bluez"
	mkdir -p "$subpkgdir"
	mv "$pkgdir"/lib "$subpkgdir"/
}

deprecated() {
	pkgdesc="Deprecated bluetooth tools"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ciptool \
		"$pkgdir"/usr/bin/hciattach \
		"$pkgdir"/usr/bin/hciconfig \
		"$pkgdir"/usr/bin/hcidump \
		"$pkgdir"/usr/bin/hcitool \
		"$pkgdir"/usr/bin/rfcomm \
		"$pkgdir"/usr/bin/sdptool \
		"$subpkgdir"/usr/bin/
}

obexd() {
	pkgdesc="Bluez OBEX daemon"
	mkdir -p "$subpkgdir"/usr/lib/bluetooth
	mv "$pkgdir"/usr/lib/bluetooth/obexd "$subpkgdir"/usr/lib/bluetooth
}

sha512sums="8b14eea98f541b981162abce728e0f917654ad3c990721ec398fe41bdd68069fe55ff64b61bc3c3b9f813facf42c995b07619f6d5d153965de27154b1a7b578f  bluez-5.51.tar.xz
fc43c78ed248ea412529eed5ae8bb47bacca9bf5b3b10de121ddd4e792c85893561a88be4aa2c6318106e5d2146a721445152d44fa60ca257ca0b4eb87318c1e  bluetooth.initd
8d7b7c8938a2316ce0a855e9bdf1ef8fcdf33d23f4011df828270a088b88b140a19c432e83fef15355d0829e3c86be05b63e7718fef88563254ea239b8dc12ac  rfcomm.initd
a70aa0dbbabe7e29ee81540a6f98bf191a850da55a28f678975635caf34b363cf4d461a801b3484120ee28fdd21240bd456a4f5d706262700924bd2e9a0972fb  rfcomm.confd
73202915fda01d420b2864da77b1c25af5a55c815e9be6546400a0940bfb6097d83322790bc22a80ec0fcd557144fdd1877e243a79285a7f040ff96ba3600b94  001-bcm43xx-Add-bcm43xx-3wire-variant.patch
d5fd1c962bd846eaa6fff879bab85f753eb367d514f82d133b5d3242e1da989af5eddd942c60a87d5b67783e060f91bfa0f74fb1e8e6699cdee6e5bbe6a431ea  002-bcm43xx-The-UART-speed-must-be-reset-after-the-firmw.patch
784e9644c8de4e2693e2eeed988a245608b8cb14e1fc0dff8795c60c527b2e8d0c87862cfbfd6b850b47ae80cdf993a5ed3f477078ea1068fd7374899c7a1a77  003-Increase-firmware-load-timeout-to-30s.patch
42ac04044a8c66e07487598b3a75ef52efc32999ebce4e7c63f6198e2f603f4a1442e74600e43a0938cb4f52d4db0298aa99050b18144b84990cda71748e9de5  004-Move-the-43xx-firmware-into-lib-firmware.patch
b7640a78cb33b3628564ebb2d8bcaf4255bddf5a25068838a9cc5fbd26d5d8dbf51e4b6b6fd2137fccae593482b2927ba13cb166be255be02d72a245ea60c3ff  bluez-5.40-obexd_without_systemd-1.patch
04c4889372c8e790bb338dde7ffa76dc32fcf7370025c71b9184fcf17fd01ade4a6613d84d648303af3bbc54043ad489f29fc0cd4679ec8c9029dcb846d7e026  disable-lock-test.patch
118d55183860f395fc4bdc93efffb13902ebf7388cad722b9061cd2860d404333e500af521741c3d92c0f8a161f6810348fbeb6682e49c372383f417aed8c76a  fix-endianness.patch"
