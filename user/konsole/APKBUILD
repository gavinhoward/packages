# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=konsole
pkgver=19.08.2
pkgrel=0
pkgdesc="Terminal emulator for Qt/KDE"
url="https://konsole.kde.org/"
arch="all"
options="!check"  # Requires running DBus session bus.
license="GPL-2.0-only AND LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kbookmarks-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kguiaddons-dev kdbusaddons-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev knotifications-dev knotifyconfig-dev kparts-dev
	kpty-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev python3
	kwindowsystem-dev kxmlgui-dev kdbusaddons-dev knewstuff-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/konsole-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b781e1e6a19b81a49722fa04d568a53dda2c22f72ee197ebf7bacb9142d260ce3071a12cfada905d029dc2ab43a047205175cde5906a4033fb8bdd20876c1af5  konsole-19.08.2.tar.xz"
