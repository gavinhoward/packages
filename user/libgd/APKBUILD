# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=libgd
pkgver=2.2.5
pkgrel=1
pkgdesc="Library for dynamic image creation"
url="http://libgd.github.io/"
arch="all"
options="!check"  # Upstream bug 201 regression.
license="MIT"
depends=""
makedepends="bash fontconfig-dev freetype-dev libjpeg-turbo-dev libpng-dev
	libwebp-dev zlib-dev"
subpackages="$pkgname-dev"
replaces="gd"
source="https://github.com/$pkgname/$pkgname/releases/download/gd-$pkgver/$pkgname-$pkgver.tar.xz
	CVE-2016-7568.patch
	CVE-2018-5711.patch
	CVE-2018-1000222.patch
	CVE-2019-6977.patch
	CVE-2019-6978.patch
	"

# secfixes:
#   2.2.5-r1:
#     - CVE-2018-5711
#     - CVE-2018-1000222
#     - CVE-2019-6977
#     - CVE-2019-6978

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-werror
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	depends="$pkgname perl"
	replaces="gd-dev"
	mv "$pkgdir"/usr/bin/bdftogd "$subpkgdir"/usr/bin
}

sha512sums="e4598e17a277a75e02255402182cab139cb3f2cffcd68ec05cc10bbeaf6bc7aa39162c3445cd4a7efc1a26b72b9152bbedb187351e3ed099ea51767319997a6b  libgd-2.2.5.tar.xz
8310d11a2398e8617c9defc4500b9ce3897ac1026002ffa36000f1d1f8df19336005e8c1f6587533f1d787a4a54d7a3a28ad25bddbc966a018aedf4d8704a716  CVE-2016-7568.patch
d6577566814cbe2d93b141a4216b32acdeb2989dc1712eb137565081b913151bbb4c69911c96b2bb7c90695078a85152d368aad183de494d1283fde25021751b  CVE-2018-5711.patch
d12462f1b159d50b9032435e9767a5d76e1797a88be950ed33dda7aa17005b7cb60560d04b9520e46d8111e1669d42ce28cb2c508f9c8825d545ac0335d2a10b  CVE-2018-1000222.patch
df84e469515f684d79ebad163e137401627310a984ac1ae6a4d31b739b3dc6d9144f101e9bfc3211af1d7cdbaa827721d21a9fe528e69b9b60a943ec8a7ab74b  CVE-2019-6977.patch
3bf31941365a878bef899afa14a89e4ad0fbfb3280d34b2118c8484698e15eff600751ae3ce146a4f006e6c21730cb18899bae3538f6cc2651025274b40cf1ca  CVE-2019-6978.patch"
