# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=libksysguard
pkgver=5.12.8
pkgrel=0
pkgdesc="KDE system monitor library"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 session
license="LGPL-2.1+ AND (GPL-2.0-only OR GPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 libx11-dev zlib-dev
	qt5-qtscript-dev ki18n-dev kauth-dev kcompletion-dev kconfigwidgets-dev
	kcoreaddons-dev kiconthemes-dev plasma-framework-dev kservice-dev
	kwindowsystem-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/libksysguard-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9913b5491ad8305efa20e933db3a108da93b83323cde33cdef24f3b40beadb166de99cbd5cbde033d82de913c5a66b493860f9d14351a7b20c401bd1a5648039  libksysguard-5.12.8.tar.xz"
