# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kdeplasma-addons
pkgver=5.12.8
pkgrel=0
pkgdesc="Extra applets and toys for KDE Plasma"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.1-only"
depends="qt5-qtquickcontrols qt5-qtquickcontrols2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtx11extras-dev kactivities-dev kconfig-dev kconfigwidgets-dev
	kcmutils-dev kcoreaddons-dev kdoctools-dev ki18n-dev knewstuff-dev
	kross-dev krunner-dev kservice-dev kunitconversion-dev
	kdelibs4support-dev plasma-framework-dev plasma-workspace-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild \
		.
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="3b0a92a90770c718de4065ad9e6f2e7937aa047ecea32ba7c28bf2324d9ad2cd2c825e6bc21a37d494b1d2e5409ba2b4efeaae2be811bee09c9ece2cc3a6941f  kdeplasma-addons-5.12.8.tar.xz"
