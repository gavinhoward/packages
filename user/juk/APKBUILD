# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=juk
pkgver=19.08.2
pkgrel=0
pkgdesc="KDE Jukebox"
url="https://juk.kde.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kcompletion-dev kcrash-dev kglobalaccel-dev ki18n-dev
	kiconthemes-dev kdoctools-dev kio-dev kjobwidgets-dev ktextwidgets-dev
	knotifications-dev kxmlgui-dev kwallet-dev kwidgetsaddons-dev
	kwindowsystem-dev taglib-dev phonon-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/juk-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1de7d1840eb41e47e29001e1031ad9f31942b0663ba76e83ece84534ca88a22ad03b5482276aa40259a72282e73241b872ff27ef81c1f777e31265564635592b  juk-19.08.2.tar.xz"
