# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Brandon Bergren <git@bdragon.rtk0.net>
pkgname=py3-sphinxcontrib-jsmath
_pkgname=sphinxcontrib-jsmath
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=1.0.1
pkgrel=0
pkgdesc="A sphinx extension which renders display math in HTML via JavaScript"
url="https://pypi.python.org/pypi/sphinxcontrib-jsmath"
arch="noarch"
options="!check" # Cyclic dependency on sphinx
license="BSD-2-Clause"
depends="python3"
makedepends="python3-dev"
checkdepends="py3-sphinx py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	python3 setup.py build
}

check() {
	cd "$builddir"
	PYTHONWARNINGS="all,ignore::DeprecationWarning:docutils.io" pytest --durations 25
}

package() {
	cd "$builddir"
	python3 setup.py install --prefix=/usr --root="$pkgdir"

}

sha512sums="c1e6488f5c0ca4567c27ec7c597c9db321ac32ce354c4ad62fea534b2ae1c0acb183a921f46216bbc3891f14acfaac05ddf324b8fdaf99828df07bc91aa7e5c7  py3-sphinxcontrib-jsmath-1.0.1.tar.gz"
