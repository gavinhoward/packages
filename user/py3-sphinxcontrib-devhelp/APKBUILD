# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Brandon Bergren <git@bdragon.rtk0.net>
pkgname=py3-sphinxcontrib-devhelp
_pkgname=sphinxcontrib-devhelp
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=1.0.1
pkgrel=0
pkgdesc="A sphinx extension to output Devhelp documents"
url="https://pypi.python.org/pypi/sphinxcontrib-devhelp"
arch="noarch"
options="!check" # Cyclic dependency with sphinx
license="BSD-2-Clause"
depends="python3"
makedepends="python3-dev"
checkdepends="py3-sphinx py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	python3 setup.py build
}

check() {
	cd "$builddir"
	PYTHONWARNINGS="all,ignore::DeprecationWarning:docutils.io" pytest --durations 25
}

package() {
	cd "$builddir"
	python3 setup.py install --prefix=/usr --root="$pkgdir"

}

sha512sums="135d6405e330cc245391646d4055988cf762d82f6473e58c64a76f3a88e7f33a66baba366e7f29e1378320bd0c374af92b0f972bceeec8c05ad0fe652a81e984  py3-sphinxcontrib-devhelp-1.0.1.tar.gz"
