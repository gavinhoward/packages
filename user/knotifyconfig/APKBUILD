# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knotifyconfig
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for configuring notifications"
url="https://www.kde.org/"
arch="all"
options="!check"  # No test suite, despite mentioning testing deps...
license="LGPL-2.0-only"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev kcompletion-dev kconfig-dev ki18n-dev kio-dev
	knotifications-dev phonon-dev"
checkdepends="kconfigwidgets-dev knotifications-dev kwidgetsaddons-dev
	kxmlgui-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifyconfig-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="f9ae7eab977be273ed12a56e903b324986794d286409aed657af22c09ba7e623f3590779916481a4c80f606abeb9ab37cf544e74a9254290b10b89abb43c7573  knotifyconfig-5.54.0.tar.xz"
