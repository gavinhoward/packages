# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmediaplayer
pkgver=5.54.0
pkgrel=0
pkgdesc="Media player framework for KDE 5"
url="https://www.kde.org/"
arch="all"
license="X11 AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kparts-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kmediaplayer-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# viewtest requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E viewtest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c49db70a8bc7a27cb49917eb83fa9e15666d25b5e641babc0af424910c73ae646d63767c5c5bef28db60489bfa52456d736e0d6f0ae04b1fd2a4d8d3027e1ba7  kmediaplayer-5.54.0.tar.xz"
