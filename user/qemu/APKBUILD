# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qemu
pkgver=3.0.0
pkgrel=5
pkgdesc="Machine emulator and virtualisation software"
url="https://www.qemu.org/"
arch="all"
license="GPL-2.0-only AND LGPL-2.1-only"
makedepends="
	alsa-lib-dev
	bison
	curl-dev
	flex
	glib-dev
	glib-static
	gnutls-dev
	gtk+3.0-dev
	icu-dev
	icu-static
	libaio-dev
	libcap-dev
	libcap-ng-dev
	libjpeg-turbo-dev
	libnfs-dev
	libpng-dev
	libssh2-dev
	libusb-dev
	libx11-dev
	libxml2-dev
	linux-headers
	lzo-dev
	ncurses-dev
	python3
	snappy-dev
	spice-dev
	texinfo
	usbredir-dev
	util-linux-dev
	vde2-dev
	xfsprogs-dev
	zlib-dev
	"
pkggroups="qemu"
install="$pkgname.pre-install $pkgname.post-install"
# suid needed for qemu-bridge-helper
# strip fails on .img files
# some tests does not run on our builders
options="suid !strip !check"
subpackages="$pkgname-doc $pkgname-lang $pkgname-guest-agent:guest
	$pkgname-user $pkgname-system"

_user_subsystems="
	aarch64
	aarch64_be
	alpha
	arm
	armeb
	cris
	hppa
	i386
	m68k
	microblaze
	microblazeel
	mips
	mips64
	mips64el
	mipsel
	mipsn32
	mipsn32el
	nios2
	or1k
	ppc
	ppc64
	ppc64abi32
	ppc64le
	riscv32
	riscv64
	s390x
	sh4
	sh4eb
	sparc
	sparc32plus
	sparc64
	tilegx
	x86_64
	xtensa
	xtensaeb"
_system_subsystems="
	system-aarch64
	system-alpha
	system-arm
	system-cris
	system-hppa
	system-i386
	system-lm32
	system-m68k
	system-microblaze
	system-microblazeel
	system-mips
	system-mips64
	system-mips64el
	system-mipsel
	system-moxie
	system-nios2
	system-or1k
	system-ppc
	system-ppc64
	system-ppcemb
	system-riscv32
	system-riscv64
	system-s390x
	system-sh4
	system-sh4eb
	system-sparc
	system-sparc64
	system-tricore
	system-unicore32
	system-x86_64
	system-xtensa
	system-xtensaeb
	"
_subsystems="$_user_subsystems $_system_subsystems"
for _sub in $_subsystems; do
	subpackages="$subpackages $pkgname-$_sub:_subsys"
done

case "$CARCH" in
	x86) _arch=i386 ;;
	x86_64) _arch=x86_64 ;;
	ppc64) _arch=ppc64 ;;
	ppc) _arch=ppc ;;
	*) _arch="" ;;
esac
if [ -n "$_arch" ]; then
	subpackages="$subpackages $pkgname-gtk"
	gtk() { _subsys system-$_arch-gtk; }
fi

subpackages="$subpackages $pkgname-img"  # -img must be declared the last

source="https://download.qemu.org/$pkgname-$pkgver.tar.xz
	0001-elfload-load-PIE-executables-to-right-address.patch
	0001-linux-user-fix-build-with-musl-on-aarch64.patch
	musl-F_SHLCK-and-F_EXLCK.patch
	fix-sigevent-and-sigval_t.patch
	xattr_size_max.patch
	ncurses.patch
	ignore-signals-33-and-64-to-allow-golang-emulation.patch
	0001-linux-user-fix-build-with-musl-on-ppc64le.patch
	fix-sockios-header.patch
	test-crypto-ivgen-skip-essiv.patch
	ppc32-musl-support.patch
	signal-fixes.patch
	sysinfo-header.patch
	fix-lm32-underlinking.patch

	$pkgname-guest-agent.confd
	$pkgname-guest-agent.initd
	80-kvm.rules
	bridge.conf
	"
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   2.8.1-r1:
#   - CVE-2016-7994
#   - CVE-2016-7995
#   - CVE-2016-8576
#   - CVE-2016-8577
#   - CVE-2016-8578
#   - CVE-2016-8668
#   - CVE-2016-8909
#   - CVE-2016-8910
#   - CVE-2016-9101
#   - CVE-2016-9102
#   - CVE-2016-9103
#   - CVE-2016-9104
#   - CVE-2016-9105
#   - CVE-2016-9106
#   - CVE-2017-2615
#   - CVE-2017-2620
#   - CVE-2017-5525
#   - CVE-2017-5552
#   - CVE-2017-5578
#   - CVE-2017-5579
#   - CVE-2017-5667
#   - CVE-2017-5856
#   - CVE-2017-5857
#   - CVE-2017-5898
#   - CVE-2017-5931

prepare() {
	default_prepare  # apply patches

	sed -i 's/^VL_LDFLAGS=$/VL_LDFLAGS=-Wl,-z,execheap/' \
		Makefile.target
}

_compile_common() {
	# -lx11 needed for qemu-system-lm32
	"$builddir"/configure \
		--prefix=/usr \
		--localstatedir=/var \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib/qemu \
		--disable-glusterfs \
		--enable-debug-info \
		--disable-strip \
		--disable-bsd-user \
		--disable-werror \
		--disable-sdl \
		--disable-xen \
		--disable-gcrypt \
		--cc="${CC:-gcc}" \
		--python="/usr/bin/python3" \
		"$@"
	make ARFLAGS="rc"
}

_compile_system() {
	_compile_common \
		--audio-drv-list=oss,alsa \
		--enable-debug-tcg \
		--enable-kvm \
		--enable-vde \
		--enable-virtfs \
		--enable-curl \
		--enable-cap-ng \
		--enable-linux-aio \
		--enable-usb-redir \
		--enable-libssh2 \
		--enable-vhost-net \
		--enable-snappy \
		--enable-tpm \
		--enable-libnfs \
		--enable-lzo \
		--enable-docs \
		--enable-curses \
		--enable-pie \
		--disable-linux-user \
		"$@"
}

build() {
	local systems
	mkdir -p "$builddir"/build \
		"$builddir"/build-user \
		"$builddir"/build-gtk

	cd "$builddir"/build-user
	_compile_common \
		--enable-linux-user \
		--disable-system \
		--static

	cd "$builddir"/build
	_compile_system \
		--enable-vnc \
		--enable-vnc-png \
		--enable-vnc-jpeg \
		--enable-spice \
		--enable-guest-agent \
		--disable-gtk

	if [ -n "$_arch" ]; then
		cd "$builddir"/build-gtk
		_compile_system \
			--enable-gtk \
			--with-gtkabi=3.0 \
			--disable-vnc \
			--disable-spice \
			--disable-guest-agent \
			--target-list="$_arch-softmmu"
	fi
}

check() {
	cd "$builddir"/build

	# XXX: ESSIV crypto tests are disabled, see test-crypto-ivgen-skip-essiv.patch.
	make check V=1
}

package() {
	cd "$builddir"/build-user
	make DESTDIR="$pkgdir" install

	cd "$builddir"/build
	make DESTDIR="$pkgdir" install

	install -Dm640 -g qemu "$srcdir"/bridge.conf \
		"$pkgdir"/etc/qemu/bridge.conf

	install -Dm644 "$srcdir"/80-kvm.rules \
		"$pkgdir"/lib/udev/rules.d/80-kvm.rules

	# qemu-bridge-helper needs suid to create tunX devices;
	# allow only users in the qemu group to run it.
	chmod 04710 "$pkgdir"/usr/lib/qemu/qemu-bridge-helper
	chgrp qemu "$pkgdir"/usr/lib/qemu/qemu-bridge-helper

	if [ -n "$_arch" ]; then
		cd "$builddir"/build-gtk
		install $_arch-softmmu/qemu-system-$_arch \
			"$pkgdir"/usr/bin/qemu-system-$_arch-gtk
	fi

	# Do not install HTML docs.
	rm "$pkgdir"/usr/share/doc/qemu/*.html
}

_subsys() {
	local name="${1:-"${subpkgname#$pkgname-}"}"
	pkgdesc="Qemu $(printf '%s' "$name" | tr - ' ') emulator"
	options=""
	depends=""
	case "$name" in
		system*) depends="qemu";;
	esac

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/qemu-$name "$subpkgdir"/usr/bin/
}

img() {
	pkgdesc="QEMU command line tool for manipulating disk images"
	depends=""
	options=""

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/qemu-img \
		"$pkgdir"/usr/bin/qemu-io \
		"$pkgdir"/usr/bin/qemu-nbd \
		"$subpkgdir"/usr/bin/

	# We exploit the fact that -img subpackage are created last
	# and check that we done have new systems that belongs in
	# subpackage.
	local path= retval=0
	for path in "$pkgdir"/usr/bin/qemu-system-*; do
		if [ -r "$path" ]; then
			error "Please create a subpackage for ${path##*/}"
			retval=1
		fi
	done
	return $retval
}

dbg() {
	# God, save me.
	local _save="$srcdir"/tmpshare
	# default_dbg uses srcdir for its own purposes;
	# its value cannot be used after it is executed
	mv "$pkgdir"/usr/share/qemu $_save
	default_dbg
	mv $_save "$pkgdir"/usr/share/qemu
}

user() {
	pkgdesc="All QEMU user targets (metapackage)"
	depends=""
	local u
	for u in $_user_subsystems; do
		depends="qemu-$u $depends"
	done
	mkdir -p "$subpkgdir"
}

system() {
	pkgdesc="All QEMU system targets (metapackage)"
	depends=""
	local s
	for s in $_system_subsystems; do
		depends="qemu-$s $depends"
	done
	mkdir -p "$subpkgdir"
}

guest() {
	pkgdesc="QEMU guest agent"
	depends=""
	options=""

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/qemu-ga "$subpkgdir"/usr/bin/

	install -Dm755 "$srcdir"/$pkgname-guest-agent.initd \
		"$subpkgdir"/etc/init.d/$pkgname-guest-agent
	install -Dm644 "$srcdir"/$pkgname-guest-agent.confd \
		"$subpkgdir"/etc/conf.d/$pkgname-guest-agent
}

sha512sums="a764302f50b9aca4134bbbc1f361b98e71240cdc7b25600dfe733bf4cf17bd86000bd28357697b08f3b656899dceb9e459350b8d55557817444ed5d7fa380a5a  qemu-3.0.0.tar.xz
405008589cad1c8b609eca004d520bf944366e8525f85a19fc6e283c95b84b6c2429822ba064675823ab69f1406a57377266a65021623d1cd581e7db000134fd  0001-elfload-load-PIE-executables-to-right-address.patch
1ac043312864309e19f839a699ab2485bca51bbf3d5fdb39f1a87b87e3cbdd8cbda1a56e6b5c9ffccd65a8ac2f600da9ceb8713f4dbba26f245bc52bcd8a1c56  0001-linux-user-fix-build-with-musl-on-aarch64.patch
224f5b44da749921e8a821359478c5238d8b6e24a9c0b4c5738c34e82f3062ec4639d495b8b5883d304af4a0d567e38aa6623aac1aa3a7164a5757c036528ac0  musl-F_SHLCK-and-F_EXLCK.patch
5da8114b9bd2e62f0f1f0f73f393fdbd738c5dea827ea60cedffd6f6edd0f5a97489c7148d37a8ec5a148d4e65d75cbefe9353714ee6b6f51a600200133fc914  fix-sigevent-and-sigval_t.patch
4b1e26ba4d53f9f762cbd5cea8ef6f8062d827ae3ae07bc36c5b0c0be4e94fc1856ad2477e8e791b074b8a25d51ed6d0ddd75e605e54600e5dd0799143793ce4  xattr_size_max.patch
b6ed02aaf95a9bb30a5f107d35371207967edca058f3ca11348b0b629ea7a9c4baa618db68a3df72199eea6d86d14ced74a5a229d17604cc3f0adedcfeae7a73  ncurses.patch
fd178f2913639a0c33199b3880cb17536961f2b3ff171c12b27f4be6bca032d6b88fd16302d09c692bb34883346babef5c44407a6804b20a39a465bb2bc85136  ignore-signals-33-and-64-to-allow-golang-emulation.patch
d8933df9484158c2b4888254e62117d78f8ed7c18527b249419f39c2b2ab1afa148010884b40661f8965f1ef3105580fceffdfddbb2c9221dc1c62066722ba65  0001-linux-user-fix-build-with-musl-on-ppc64le.patch
39590476a4ebd7c1e79a4f0451b24c75b1817a2a83abaa1f71bb60b225d772152f0af8f3e51ff65645e378c536ffa6ff551dade52884d03a14b7c6a19c5c97d4  fix-sockios-header.patch
8b8db136f78bd26b5da171effa9e11016ec2bc3e2fc8107228b5543b47aa370978ed883794aa4f917f334e284a5b49e82070e1da2d31d49301195b6713a48eff  test-crypto-ivgen-skip-essiv.patch
fb0130fa4e8771b23ae337ea3e5e29fd5f7dcfe7f9f7a68968f5b059bb4dd1336b0d04c118840d55885bc784a96a99b28aeacbc6a5549b2e6750c9d3099a897c  ppc32-musl-support.patch
c6436b1cc986788baccd5fe0f9d23c7db9026f6b723260611cf894bd94ee830140a17ee5859efe0dad0ca3bfe9caae1269bc5c9ab4c6e696f35c7857c1b5c86b  signal-fixes.patch
698f6b134f4ca87f4de62caf7a656841a40a451b8686ca95928f67a296e58a7493d432d9baa5f6360917865aa4929600baf1699993b0600923a066ca9d45d1da  sysinfo-header.patch
2828cc612539aa93b5789de7de6d4f85d3cf82311484c0fe91fdd3efeb972057e2baa2a3809ed633d6caa1785642d49196cb282b095d7553c510c47ce7d6a702  fix-lm32-underlinking.patch
d90c034cae3f9097466854ed1a9f32ab4b02089fcdf7320e8f4da13b2b1ff65067233f48809911485e4431d7ec1a22448b934121bc9522a2dc489009e87e2b1f  qemu-guest-agent.confd
1cd24c2444c5935a763c501af2b0da31635aad9cf62e55416d6477fcec153cddbe7de205d99616def11b085e0dd366ba22463d2270f831d884edbc307c7864a6  qemu-guest-agent.initd
9b7a89b20fcf737832cb7b4d5dc7d8301dd88169cbe5339eda69fbb51c2e537d8cb9ec7cf37600899e734209e63410d50d0821bce97e401421db39c294d97be2  80-kvm.rules
749efa2e764006555b4fd3a8e2f6d1118ad2ea4d45acf99104a41a93cfe66dc9685f72027c17d8211e5716246c2a52322c962cf4b73b27541b69393cd57f53bb  bridge.conf"
