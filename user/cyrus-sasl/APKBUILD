# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: 
pkgname=cyrus-sasl
pkgver=2.1.27
pkgrel=0
pkgdesc="Cyrus Simple Authentication Service Layer (SASL)"
url="https://www.cyrusimap.org/sasl/"
arch="all"
options="!check"  # No test suite.
license="BSD-4-Clause"
subpackages="$pkgname-dev $pkgname-doc $pkgname-gssapi $pkgname-gs2
	$pkgname-scram $pkgname-ntlm $pkgname-crammd5 $pkgname-digestmd5
	libsasl $pkgname-openrc"
depends=""
makedepends="db-dev openssl-dev krb5-dev
	autoconf automake libtool"
source="https://github.com/cyrusimap/$pkgname/releases/download/$pkgname-$pkgver/$pkgname-$pkgver.tar.gz
	saslauthd.initd
	"

# secfixes:
#   2.1.26-r7:
#   - CVE-2013-4122

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-anon \
		--enable-cram \
		--enable-digest \
		--enable-login \
		--enable-ntlm \
		--disable-otp \
		--enable-plain \
		--with-gss_impl=mit \
		--with-devrandom=/dev/urandom \
		--without-ldap \
		--with-saslauthd=/var/run/saslauthd \
		--mandir=/usr/share/man
	# parallel build is broken
	make -j1
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	install -D -m644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

	install -Dm755 ../saslauthd.initd "$pkgdir"/etc/init.d/saslauthd
	install -d "$pkgdir"/var/run/saslauthd
}

_plugindir=usr/lib/sasl2
_plugin() {
	depends=
	replaces="libsasl"
	pkgdesc="Cyrus SASL plugin for $2"
	mkdir -p "$subpkgdir"/$_plugindir
	mv "$pkgdir"/$_plugindir/lib${1}.so* "$subpkgdir"/$_plugindir/
}

gssapi() { _plugin gssapiv2 "Kerberos (GSSAPI)"; }
gs2() { _plugin gs2 GS2; }
scram() { _plugin scram SCRAM; }
ntlm() { _plugin ntlm NTLM; }
crammd5() { _plugin crammd5 CRAM-MD5; }
digestmd5() { _plugin digestmd5 DIGEST-MD5; }

libsasl() {
	depends=
	pkgdesc="Cyrus Simple Authentication and Security Layer (SASL) library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
}

sha512sums="d11549a99b3b06af79fc62d5478dba3305d7e7cc0824f4b91f0d2638daafbe940623eab235f85af9be38dcf5d42fc131db531c177040a85187aee5096b8df63b  cyrus-sasl-2.1.27.tar.gz
71a00a22f91f0fb6ba2796acede321a0f071b1d7a99616f0e36c354213777f30575c340b6df392dcbfc103ba7640d046144882f6a7b505f59709bb5c429b44d8  saslauthd.initd"
