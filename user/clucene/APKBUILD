# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=clucene
pkgver=2.3.3.4
pkgrel=5
pkgdesc="A C++ port of Lucene"
url="http://www.sourceforge.net/projects/clucene"
arch="all"
license="(LGPL-2.1+ OR Apache-2.0) AND RSA-MD AND (LGPL-2.0+ OR Apache-2.0) AND Zlib AND Apache-2.0"
depends=""
depends_dev="zlib-dev boost-dev"
makedepends="cmake $depends_dev"
subpackages="$pkgname-dev $pkgname-contribs"
source="http://downloads.sourceforge.net/clucene/clucene-core-$pkgver.tar.gz
	pkgconfig.patch
	install_contribs_lib.patch
	usleep.patch
	disable-threading.patch
"
builddir="$srcdir/$pkgname-core-$pkgver"

prepare() {
	default_prepare
	cd "$builddir"
	# Do not use bundled zlib or boost
	sed -ie '/ADD_SUBDIRECTORY (src\/ext)/d' CMakeLists.txt
	rm -rf src/ext
}

build() {
	cd "$builddir"
	cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_CONTRIBS_LIB=BOOL:ON \
		-DLIB_DESTINATION:PATH=/usr/lib \
		-DLUCENE_SYS_INCLUDES:PATH=/usr/lib \
		-DDISABLE_MULTITHREADING=BOOL:ON
	make
}

check() {
	cd "$builddir"

	# clucene is not smart enough to delete files between successive checks,
	# so let's remove them for it. Otherwise, if check is run a second time
	# without clearing the files beforehand, a lot of tests will fail.
	rm -rf "$builddir/tmp"
	mkdir "$builddir/tmp"

	# Need to force cmake to update - some sort of race condition
	touch CMakeCache.txt
	make cl_test

	cd bin
	TMP="$builddir/tmp" ./cl_test
}

package() {
	cd "$builddir"
	make install/fast DESTDIR="$pkgdir"
	rm -r "$pkgdir"/usr/lib/CLuceneConfig.cmake
}

contribs() {
	pkgdesc="Language specific text analyzers for CLucene"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libclucene-contribs*.so.* "$subpkgdir"/usr/lib/
}

dev()  {
	default_dev
	mv "$pkgdir"/usr/lib/CLucene/*.cmake "$subpkgdir"/usr/lib/CLucene/
}

sha512sums="1c9da9077edcebd46563bd9e47d330518e0b30061016650a759cfe051e9748fdad8932a472b1cca53a6adafed5f41656527271fc5f55ddfcefb558f0d83286b4  clucene-core-2.3.3.4.tar.gz
b357cb5a1c5d66219f3168724a71af3ebf5c45c752a612d3a69c170b739acc065dc17c261c3a730298ea6c637fe820637a100f73ab03d931734f80bb598fbf55  pkgconfig.patch
0aa92635949089196e4e7c579e78761e8751987ef5036320a161a4aaa67da3c63756398c903419c76ea0fbdc8a949e871fcb65be98179a9853a24a5a4cacfde3  install_contribs_lib.patch
5de49cb5eba1f9033f28281e0e5ff0e29fe6842ea3e1cb246c12649dc12dedad15e1811b45ccc989a1b743657eb0eeb4937616aa19ec5ee482cd323f54431b99  usleep.patch
3ded79aed693f6de35389377eddea0b4b0bb0cafd016f6f766986297c35912b18c1e90cedb1fb1d256765abdaab05ca2999aec2fe7ae309fa75231ac94c519a9  disable-threading.patch"
