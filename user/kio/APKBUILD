# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kio
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for data and file management"
url="https://www.kde.org/"
arch="all"
options="!checkroot !check"  # Test requires X11; segfault bug #34
license="LGPL-2.1-only AND LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev attica-dev kbookmarks-dev kcompletion-dev
	kconfig-dev kcoreaddons-dev kiconthemes-dev kitemviews-dev
	kjobwidgets-dev kservice-dev kwidgetsaddons-dev solid-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev python3
	qt5-qttools-dev doxygen graphviz karchive-dev kdbusaddons-dev ki18n-dev
	knotifications-dev kwallet-dev acl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kio-$pkgver.tar.xz
	strcpy-ppc.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="163929bd8e6e4d3be5390090511cafd11213e476a0460e5fdcffde15181332545380ef25f6350e465dbc7f9872a6b771e891f5302af64bd3ced162b2a94a8c6d  kio-5.54.0.tar.xz
280b2a610613786e688c06f4d4b51d3b9d583472bf1941b7b5873fece31b5df0b2f8f08f8de677ebb39fc906c57a2fa50d7890105c0aeb930b82986e0b504623  strcpy-ppc.patch"
