# Contributor: Luis Ressel <aranea@aixah.de>
# Maintainer: Luis Ressel <aranea@aixah.de>
pkgname=unbound
pkgver=1.9.4
pkgrel=0
pkgdesc="A validating, recursive and caching DNS resolver"
url="https://nlnetlabs.nl/projects/unbound/about/"
arch="all"
options="!checkroot"
license="BSD-3-Clause"
depends=""
makedepends="expat-dev libevent-dev openssl-dev python3-dev swig"
subpackages="$pkgname-dev $pkgname-doc $pkgname-openrc $pkgname-python"
install="$pkgname.pre-install"
pkgusers="unbound"
pkggroups="unbound"
source="https://nlnetlabs.nl/downloads/$pkgname/$pkgname-$pkgver.tar.gz
	unbound.confd
	unbound.initd"

build() {
	# cachedb, dnscrypt and dnstap have yet unpackaged dependencies
	PYTHON_VERSION=3 ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-rpath \
		--with-rootkey-file=/etc/unbound/var/root.key \
		--with-pidfile=/run/unbound.pid \
		--enable-tfo-client \
		--enable-tfo-server \
		--disable-cachedb \
		--disable-dnscrypt \
		--disable-dnstap \
		--enable-ipsecmod \
		--enable-subnet \
		--with-pthreads \
		--with-pyunbound \
		--without-pythonmodule \
		--with-ssl=/usr \
		--with-libevent=/usr \
		--with-libexpat=/usr
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	install -do unbound "$pkgdir/etc/unbound/var"
}

openrc() {
	default_openrc
	install -Dm755 "$srcdir/unbound.initd" "$subpkgdir/etc/init.d/unbound"
	install -Dm644 "$srcdir/unbound.confd" "$subpkgdir/etc/conf.d/unbound"
}

python() {
	pkgdesc="$pkgdesc (Python bindings)"

	install -d "$subpkgdir/usr/lib"
	mv "$pkgdir/usr/lib/python"* "$subpkgdir"
}

sha512sums="44021014c944fc01a1f5f9afd77145f5554a3282cc2bfd54526fc4f88346f497c847ddb72bafa155d7e6e5dd02b6bb031836ead4408977d4e4b5b3290dffea9c  unbound-1.9.4.tar.gz
de9dc269553f5449c1757690c2a8a3b9f228964f5672d721cfdbc29e6fab8954907fa4fa3761dd6f705b3ccd2f729cc6e2fe870107feb68cca611589f8306b94  unbound.confd
6627cbcbcb4e04f596e7e0ffdbf80ceb54cdb59144cb89896e53503dfb41b99fd77d8b85d05e6670f77023b6eafec8885b00c0c8e4e34e3e638c52c037a7985e  unbound.initd"
