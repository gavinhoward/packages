# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=sddm-kcm
pkgver=5.12.8
pkgrel=0
pkgdesc="KDE configuration applet for SDDM"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qtdeclarative-dev kcoreaddons-dev ki18n-dev kxmlgui-dev kauth-dev
	kconfigwidgets-dev kio-dev libxcb-dev xcb-util-image-dev libxcursor-dev
	knewstuff-dev"
install_if="systemsettings sddm"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz
	qt59.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dbf341868837044a12d50c9292fefaf6a28cd3da48b91a0516f9709c63124ab2ccc73e3472d78dd41209fd07532126bb94059d1b27bf877e61deb1b3cbd5ac64  sddm-kcm-5.12.8.tar.xz
698bdd605b96655dc2038378cb3a749101c0dcc19cf7ba996f4d64ec6281e8541efb313b8dbf6095181766ab5a5bb3d2fecd25267e61b848dc4f08f89cde9bde  qt59.patch"
