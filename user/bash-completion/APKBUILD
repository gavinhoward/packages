# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Kiyoshi Aman <kiyoshi.aman@gmail.com>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer:
pkgname=bash-completion
pkgver=2.9
pkgrel=0
pkgdesc="Command-line tab-completion for the Bash shell"
url="https://github.com/scop/bash-completion"
arch="noarch"
license="GPL-2.0+"
depends="bash"
makedepends=""
checkdepends="py3-pexpect py3-pytest"
subpackages="$pkgname-doc"
source="https://github.com/scop/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.xz
	fix-tests.patch
	"

# Provided with util-linux and networkmanager:
_conflicting="
	cal
	chsh
	dmesg
	eject
	hd
	hexdump
	hwclock
	ionice
	look
	ncal
	newgrp
	renice
	rfkill
	rtcwake
	su
	nmcli
	umount
	mount
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 AUTHORS CHANGES CONTRIBUTING.md README.md \
		"$pkgdir"/usr/share/doc/$pkgname/

	cd "$pkgdir"/usr/share/bash-completion/completions
	for c in $_conflicting; do
		rm -f $c
	done
}

sha512sums="e864091196d670699bdb2af3fc40464788e79c932fa564afa7ba34a637aa1583db7dbceab0e7ba6718fac99e9fd2dfb03d1ee51d7cf279d925ad63f60401d7d5  bash-completion-2.9.tar.xz
a029f83c62edea94c86008db18dfe689880e3cea144e18bfce6a0675ddc4bc6ce2f29c2189c25e29932bbaf3139ad17c166c8f304ae3b3389d6b7894bbf15ab0  fix-tests.patch"
