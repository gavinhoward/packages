# Contributor: Luis Ressel <aranea@aixah.de>
# Maintainer: Luis Ressel <aranea@aixah.de>
pkgname=notmuch
pkgver=0.28.4
pkgrel=0
pkgdesc="Thread-based email index, search and tagging"
url="https://notmuchmail.org/"
arch="all"
license="GPL-3.0+"
depends=""
depends_dev="gmime-dev talloc-dev xapian-core-dev zlib-dev"
makedepends="$depends_dev doxygen emacs python3-dev ruby-dev"
checkdepends="bash dtach gnupg"
subpackages="$pkgname-dev $pkgname-doc $pkgname-emacs py3-$pkgname:_py ruby-$pkgname:_rb $pkgname-bash-completion:bashcomp:noarch $pkgname-zsh-completion:zshcomp:noarch"
source="https://notmuchmail.org/releases/notmuch-$pkgver.tar.gz
	0.28.2-disable-rpath.patch"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-bash-completion \
		--without-desktop \
		--without-rpath
	make

	cd bindings/python
	python3 setup.py build
}

check() {
	cd "$builddir"
	# config, regexp-query: musl issues, TODO: Fixed in master
	# atomicity, count, new, insert: gdb-based tests which fail randomly on some arches, with some CFLAGS, or during some moon phases
	NOTMUCH_SKIP_TESTS="config regexp-query atomicity count new insert" make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

doc() {
	default_doc
	# TODO: We'd need sphinx to generate the man pages, but the raw *.rst's are still better than no docs at all.
	mkdir -p "$subpkgdir/usr/share/doc/$pkgname" "$builddir/doc/man"*
	cp -r "$builddir/doc/man"* "$subpkgdir/usr/share/doc/$pkgname/"
}

emacs() {
	mkdir -p "$subpkgdir/usr/bin" "$subpkgdir/usr/share"
	mv "$pkgdir/usr/bin/notmuch-emacs-mua" "$subpkgdir/usr/bin"
	mv "$pkgdir/usr/share/emacs" "$subpkgdir/usr/share"
}

_py() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="$pkgname=$pkgver-r$pkgrel python3"

	cd "$builddir/bindings/python"
	python3 setup.py install --prefix=/usr --root="$subpkgdir"
}

_rb() {
	pkgdesc="$pkgdesc (Ruby bindings)"
	depends="$pkgname=$pkgver-r$pkgrel ruby"

	cd "$builddir/bindings/ruby"
	make DESTDIR="$subpkgdir" install
}


bashcomp() {
	pkgdesc="$pkgdesc (Bash completion)"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	install -Dt "$subpkgdir/usr/share/bash-completion" "$builddir/completion/notmuch-completion.bash"
}

zshcomp() {
	pkgdesc="$pkgdesc (Zsh completion)"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	mkdir -p "$subpkgdir/usr/share"
	mv "$pkgdir/usr/share/zsh" "$subpkgdir/usr/share"
}

sha512sums="77d7ad81e1f7dc93ae6392346da434f6dc74c21d19be728c2a6ca283c429f36e7081387d223af58eb5f63f5d2a8ad8367f0103f0bb9d052890c07fe419abbee7  notmuch-0.28.4.tar.gz
b2991ee5edf927f04d904d99888dbce17a98e9d4d7d809bc2248f45d214025f4a1632b19e4fbca2b08e99721ec8a7eea82384c5c718206884801c7bfa5a8c540  0.28.2-disable-rpath.patch"
