# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kconfigwidgets
pkgver=5.54.0
pkgrel=1
pkgdesc="Framework providing widgets for software configuration"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kauth-dev kcodecs-dev kconfig-dev kguiaddons-dev
	ki18n-dev kwidgetsaddons-dev kdoctools-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz
	languagename.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c6ec2d90c3c227eb9b092bf5b33c39f99ca4f68b1337cc0655d679648710987d13e1c8d9622ad5683ba5b2b3037aca510d96ff64d04a41dd442f3bed74398b73  kconfigwidgets-5.54.0.tar.xz
3b1abbd9b5cef4400a8a962cef4f8a3614f49700fdf9df25e709a8105d9e59429102aa94b8a47fa35e7dcb9d39ba5bbfd8485743fa6f64155bef3aa1afb4bb9e  languagename.patch"
