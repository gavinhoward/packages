# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwordquiz
pkgver=19.08.2
pkgrel=0
pkgdesc="Flash card trainer for KDE"
url="https://www.kde.org/applications/education/kwordquiz/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kcrash-dev
	kconfig-dev kconfigwidgets-dev kdoctools-dev kguiaddons-dev
	kiconthemes-dev kitemviews-dev knotifyconfig-dev knewstuff-dev
	knotifications-dev kxmlgui-dev kdelibs4support-dev
	libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kwordquiz-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c8eec15289a8c7cbb11896cc3fd6178e113dad105439e0e1040ec0e919a84efc46ca4565a9a50526d7b222ef10022a130d99c4269b7073f4bd5f49dde3c8fda1  kwordquiz-19.08.2.tar.xz"
