# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=nextcloud
pkgver=14.0.13
pkgrel=0
pkgdesc="Self-hosted file sync and groupware server"
url="https://nextcloud.com"
arch="noarch"
options="!check"  # No test suite.
license="AGPL-3.0+ AND AGPL-3.0-only AND MIT AND Apache-2.0 AND (MIT OR GPL-2.0-only) AND BSD-3-Clause AND (Apache-2.0 OR GPL-2.0-only) AND GPL-3.0+ AND BSD-2-Clause AND PHP-3.0 AND (Apache-2.0 OR MPL-2.0)"
depends="
	ca-certificates
	php7
	php7-ctype
	php7-curl
	php7-dom
	php7-fileinfo
	php7-gd
	php7-iconv
	php7-intl
	php7-json
	php7-mbstring
	php7-openssl
	php7-pcntl
	php7-pdo
	php7-posix
	php7-session
	php7-simplexml
	php7-xml
	php7-xmlreader
	php7-xmlwriter
	php7-zip
	$pkgname-sqlite
"
makedepends="libxml2-utils"
provides="
	$pkgname-accessibility=$pkgver-r$pkgrel
	$pkgname-dav=$pkgver-r$pkgrel
	$pkgname-federatedfilesharing=$pkgver-r$pkgrel
	$pkgname-files=$pkgver-r$pkgrel
	$pkgname-provisioning_api=$pkgver-r$pkgrel
	$pkgname-support=$pkgver-r$pkgrel
"
install="$pkgname.pre-install $pkgname.post-upgrade
	$pkgname-initscript.post-install"
pkgusers="nextcloud"
pkggroups="www-data"
subpackages="$pkgname-doc $pkgname-initscript $pkgname-mysql $pkgname-pgsql
	$pkgname-sqlite $pkgname-default-apps:_default_apps"
source="https://download.nextcloud.com/server/releases/$pkgname-$pkgver.zip
	nextcloud14-dont-chmod.patch
	dont-update-htaccess.patch
	disable-integrity-check-as-default.patch
	iconv-ascii-translit-not-supported.patch
	use-external-docs-if-local-not-avail.patch

	$pkgname-config.php
	$pkgname.logrotate
	$pkgname.confd
	$pkgname.cron
	fpm-pool.conf
	occ
"
builddir="$srcdir/$pkgname"

# List of bundled apps to separate into subpackages. Keep it in sync!
# Note: Don't add "files", "dav", and "provisioning_api" here, these should
# be always installed.
_apps="activity
	admin_audit
	comments
	encryption
	federation
	files_external
	files_pdfviewer
	files_sharing
	files_texteditor
	files_trashbin
	files_versions
	files_videoplayer
	firstrunwizard
	gallery
	logreader
	lookup_server_connector
	nextcloud_announcements
	notifications
	oauth2
	password_policy
	serverinfo
	sharebymail
	survey_client
	systemtags
	theming
	twofactor_backupcodes
	user_external
	user_ldap
	workflowengine
"
for _i in $_apps; do
	subpackages="$subpackages $pkgname-$_i:_package_app"
done

# Directory for apps shipped with Nextcloud.
_appsdir="usr/share/webapps/$pkgname/apps"

build() {
	cd "$builddir"
}

package() {
	local basedir="var/lib/$pkgname"
	local datadir="$basedir/data"
	local wwwdir="usr/share/webapps/$pkgname"
	local confdir="etc/$pkgname"

	mkdir -p "$pkgdir/${wwwdir%/*}"
	cp -a "$builddir" "$pkgdir/$wwwdir"

	chmod +x "$pkgdir/$wwwdir/occ"
	chmod 664 "$pkgdir/$wwwdir/.htaccess" "$pkgdir/$wwwdir/.user.ini"

	# Let's not ship upstream's 'updatenotification' app and updater, which
	# has zero chance of working and a big chance of blowing things up.
	rm -r "$pkgdir/$wwwdir/apps/updatenotification" \
		"$pkgdir/$wwwdir/lib/private/Updater"

	# Replace bundled CA bundle with ours.
	ln -sf /etc/ssl/certs/ca-certificates.crt \
		"$pkgdir/$wwwdir/resources/config/ca-bundle.crt"

	install -dm 770 -o nextcloud -g www-data \
		"$pkgdir/$confdir" "$pkgdir/$datadir" "$pkgdir/$basedir/appstore"
	install -dm 775 -o nextcloud -g www-data "$pkgdir/var/log/$pkgname"

	# Create symlink from web root to site-apps, so web server can find
	# assets w/o explicit configuration for this layout.
	ln -s "/$basedir/appstore" "$pkgdir/$wwwdir/appstore"

	mv $pkgdir/$wwwdir/config/* "$pkgdir/$confdir/"
	rm -r "$pkgdir/$wwwdir/config"
	ln -s "/$confdir" "$pkgdir/$wwwdir/config"

	mkdir -p "$pkgdir/usr/share/doc/$pkgname"
	mv "$pkgdir/$wwwdir/core/doc" "$pkgdir/usr/share/doc/$pkgname/core"

	install -m 660 -o nextcloud -g www-data \
		"$srcdir/$pkgname-config.php" "$pkgdir/$confdir/config.php"

	install -Dm 644 "$srcdir/$pkgname.logrotate" "$pkgdir/etc/logrotate.d/$pkgname"
	install -Dm 755 "$srcdir/occ" "$pkgdir/usr/bin/occ"

	install -dm 700 -o nextcloud "$pkgdir/var/log/nextcloud"

	# Clean some unnecessary files.
	find "$pkgdir" -name '.gitignore' -delete \
		-o -name '.bower.json' -delete \
		-o -name 'README*' -delete \
		-o -name 'CHANGELOG*' -delete \
		-o -name 'CONTRIBUTING*' -delete
	find . -name '.github' -type d -prune -exec rm -r {} \;
}

doc() {
	default_doc

	local target="$subpkgdir/usr/share/webapps/$pkgname/core/doc"
	mkdir -p "${target%/*}"
	ln -s "/usr/share/doc/$pkgname/core" "$target"
}

initscript() {
	pkgdesc="Init script that runs Nextcloud with php-fpm"
	depends="$pkgname=$pkgver-r$pkgrel php7-fpm"

	install -Dm 644 "$srcdir/fpm-pool.conf" "$subpkgdir/etc/php/php-fpm.d/$pkgname.conf"
	install -Dm 644 "$srcdir/$pkgname.confd" "$subpkgdir/etc/conf.d/$pkgname"
	install -Dm 755 "$srcdir/$pkgname.cron" "$subpkgdir/etc/periodic/15min/$pkgname"

	mkdir -p "$subpkgdir/etc/init.d"
	ln -s php-fpm "$subpkgdir/etc/init.d/$pkgname"

	install -dm 700 -o nextcloud "$subpkgdir/var/tmp/$pkgname"
}

pgsql() {
	pkgdesc="Nextcloud PostgreSQL support"
	depends="$pkgname=$pkgver-r$pkgrel php7-pgsql php7-pdo_pgsql"
	mkdir -p "$subpkgdir"
}

sqlite() {
	pkgdesc="Nextcloud SQLite support"
	depends="$pkgname=$pkgver-r$pkgrel php7-sqlite3 php7-pdo_sqlite"
	mkdir -p "$subpkgdir"
}

mysql() {
	pkgdesc="Nextcloud MySQL support"
	depends="$pkgname=$pkgver-r$pkgrel php7-pdo_mysql"
	mkdir -p "$subpkgdir"
}

_default_apps() {
	pkgdesc="Nextcloud default apps"
	depends="$pkgname=$pkgver-r$pkgrel"

	local path; for path in $pkgdir/$_appsdir/*; do
		if grep -q '<default_enable\s*/>' "$path/appinfo/info.xml"; then
			depends="$depends $pkgname-${path##*/}"
		fi
	done

	mkdir -p "$subpkgdir"
}

_package_app() {
	local appname="${subpkgname#$pkgname-}"
	local appinfo="$pkgdir/$_appsdir/$appname/appinfo/info.xml"

	local name="$(xmllint --xpath '//info/name/text()' "$appinfo" 2>/dev/null)"
	pkgdesc="Nextcloud ${name:-$appname} app"

	case "$appname" in
	encryption) php_deps="php7-openssl";;
	files_external) php_deps="php7-ftp";;
	# TODO: add php7-imap, php7-smbclient
	user_external) php_deps="php7-ftp";;
	user_ldap) php_deps="php7-ldap";;
	esac

	case "$appname" in
	files_sharing) app_deps="$pkgname-federatedfilesharing";;
	# Announcements are delivered via the notifications pane
	nextcloud_announcements) app_deps="$pkgname-notifications";;
	# workflowengine provides admin panel to manage systemtags
	systemtags) app_deps="$pkgname-workflowengine";;
	esac

	depends="$pkgname=$pkgver-r$pkgrel $php_deps $app_deps"

	mkdir -p "$subpkgdir/$_appsdir"
	mv "$pkgdir/$_appsdir/$appname" "$subpkgdir/$_appsdir/"
}

sha512sums="ea6d688fb23bc6b6fbaa8b4c5bf8bdb8732825cf43bc3a09df288d128218b1ef5d743f042ec36d5e4813f0e0349524c2ff426abc1786cb77902ca20a3db7c392  nextcloud-14.0.13.zip
63690b8d8ffe6d4896c5b666aa9d493b501aa3e171c6557e7a003599049f0d36f266a2a257a9535dea055bca1e84208b219c6cd5e4ecd70dd064e1dd1007203a  nextcloud14-dont-chmod.patch
aef3c92497d738d6968e0f0b0d415b4953500db24ae14af41ef972665cf7eff00cb6c53dc953845fdbb389c3c965a75b8b14b9247513c05cf4130fe1cfc61731  dont-update-htaccess.patch
d2100a837fef1eeae5f706650ab4c985d9e00f61efa5526ef76c7c1f5811c3906eb6c3c13c151eff9677a0c303faab64411a5a84d6792728bc520d2c618d7d5b  disable-integrity-check-as-default.patch
de1b433c2fb2582b599cb25e718e454fc4b93543a5a60eee39a03bcccf35d281594611395bdebe02319bedd9a894507eef97010ebdfca381e0f1a09df283d375  iconv-ascii-translit-not-supported.patch
478f5cd7c5d30380ea619d3e8ec623217a06a09b27534266f00297545c7d276b068c5d984673eebc5676e8bac7f45112549498944ce3fa678ac8a69541d7c430  use-external-docs-if-local-not-avail.patch
4d01c89d5fd86190fb3bd6a5ca97bc623ec55d92cbf030c18c5811d711cea557485d334a6588e458eea4e0b3ad82f4defd7cb5a9d4f393ce4d5b32abf45db596  nextcloud-config.php
7388458a9e8b7afd3d3269718306410ffa59c3c23da4bef367a4d7f6d2570136fae9dd421b19c1441e7ffb15a5405e18bb5da67b1a15f9f45e8b98d3fda532ba  nextcloud.logrotate
dcc57735d7d4af4a7ebbdd1186d301e51d2ae4675022aea6bf1111222dfa188a3a490ebd6e7c8a7ac30046cb7d93f81cec72a51acbc60d0c10b7fb64630c637a  nextcloud.confd
921b0e5f087f24e705dce62c078dea4d2f524c40a746ed7b19f1cff3405b9ea489c10a6dbcea87be6068f575be565b77b02c9f2c3ae6a7fb85367dbe3b7300c5  nextcloud.cron
b829ed942916660065dd1030f9f35fa2f8c45a36dc791417108761c15b081777c302f305fd6490ea47d0ae41b8589c8d62e01e0f163105bd6b29fd3bed36ddcd  fpm-pool.conf
959852e34f010e635470829d66713f3e22c47717ec2c6487759eed2b6aeff9fd1421fe0271d494a02781bd1c98beb2823583623ee2cf03057cd5db794627d6c2  occ"
