# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdcraw
pkgver=19.08.2
pkgrel=0
pkgdesc="RAW image file format support for KDE"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules libraw-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/applications/$pkgver/src/libkdcraw-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b7f88ebfaa70e4bf1478b8463beea5e0d636704e91d412d973e5b9fadd7e767d8595137b2995f426180c5234f323587c1d1ab67121e71c78026d3b1f6ea59f89  libkdcraw-19.08.2.tar.xz"
