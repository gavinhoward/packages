# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kig
pkgver=19.08.2
pkgrel=0
pkgdesc="Interactive geometry learning and exploration tool"
url=" https://www.kde.org/applications/education/kig/"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kparts-dev
	qt5-qtxmlpatterns-dev ki18n-dev ktexteditor-dev kiconthemes-dev
	kconfigwidgets-dev karchive-dev kxmlgui-dev kcrash-dev kcoreaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kig-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="643fa3c0a737420ce269f1c191cba75b6f15576c72b43fdabe74d6a2196eeb099071bfa94cf61174e7e41945cd8785c8198aba36cd7878ec9b5b82b3cfcc80f1  kig-19.08.2.tar.xz"
