# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=font-adobe-utopia-type1
pkgver=1.0.4
pkgrel=0
pkgdesc="Type 1 Utopia X11 font from Adobe"
url="https://www.X.Org/"
arch="noarch"
# Okay.
# This is really hairy, but Fedora Legal says the TUG license can apply to the
# X11 distribution[1][2]; it's almost MIT style, but you have to rename the
# font if you modify it in any way.
# [1]: https://fedoraproject.org/wiki/Legal_considerations_for_fonts
# [2]: https://src.fedoraproject.org/cgit/rpms/xorg-x11-fonts.git/tree/xorg-x11-fonts.spec
license="Utopia"
depends="encodings font-alias fontconfig mkfontdir mkfontscale"
makedepends="util-macros"
subpackages=""
source="https://www.x.org/releases/individual/font/font-adobe-utopia-type1-$pkgver.tar.bz2"

prepare() {
	cd "$builddir"
	default_prepare
	update_config_sub
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name fonts.dir -exec rm {} +
	find "$pkgdir" -name fonts.scale -exec rm {} +
}

sha512sums="53ff2ce7b17824a0eed1be6c3e3386e51983389f5623e732ac898c4e08769c8720f6d6b7c75b6455e050ec5dc390376747ca4cdb9f831a218f9dd5ee5edcd0d6  font-adobe-utopia-type1-1.0.4.tar.bz2"
