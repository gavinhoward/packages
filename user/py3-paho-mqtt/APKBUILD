# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-paho-mqtt
_pkgname=paho.mqtt.python
pkgver=1.4.0
pkgrel=0
pkgdesc="MQTT version 3.1.1 client class for Python"
url="https://www.eclipse.org/paho/"
# Certified net clean
arch="noarch"
license="EPL-1.0 AND EDL-1.0"
depends="python3"
makedepends=""
checkdepends="py3-pytest"
# Use GitHub tarball since PyPI doesn't include tests
source="$pkgname-$pkgver.tar.gz::https://github.com/eclipse/paho.mqtt.python/archive/v$pkgver.tar.gz
	setup.patch"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$builddir"/src pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="2e49f0f146207ab4fbc8c346b10d3e2b50869b2d9db7c999b6645f7213fb635b64cff01c5405e7833a8b25334d24685ce6ed734a6e4b6b0660b48f65cf4a941c  py3-paho-mqtt-1.4.0.tar.gz
0cfff826651b36b5062dae8bad3abcab428dc18bfcee6c941a46f5c8900c871bd475d96fa382e06d731ea451ad9159edadf0ee3767f7dea992cb7a7ed7313d80  setup.patch"
