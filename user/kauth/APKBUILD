# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kauth
pkgver=5.54.0
pkgrel=1
pkgdesc="Framework for allowing software to gain temporary privileges"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="polkit-qt-1-dev qt5-qtbase-dev kcoreaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kauth-$pkgver.tar.xz
	CVE-2019-7443.patch"

# secfixes:
#   5.54.0-r1:
#     - CVE-2019-7443

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E KAuthHelperTest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f75c6f019d708409817a5b64d88033326a7d627cdee00e61280043d5cd8f65731f08d48405f50c7240f18670b25abfeea4b2af5966ebb2ee7e0f56669b5551c2  kauth-5.54.0.tar.xz
9cb0e37eedb5cee82c5e6d1b316f92f014c8850c9274a8d0c728f306ceabc35cbbec81b0057ebaf904bd48f3e07d6f83d91b0ef12602a0c1ba66b39a04bb45e4  CVE-2019-7443.patch"
