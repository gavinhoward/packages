# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdenlive
pkgver=19.08.1
pkgrel=0
pkgdesc="Libre video editor"
url="https://kdenlive.org/"
arch="all"
options="!check"  # keyframetest.cpp seems to be broken:
                  # it claims the KeyframeModel cannot be constructed,
		  # and then abort(3)s with an assertion in QAbstractItemModel
		  # I believe the fakeit/Mock code is subtly broken somewhere.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev kio-dev
	qt5-qtsvg-dev qt5-qtdeclarative-dev kxmlgui-dev karchive-dev kcrash-dev
	kbookmarks-dev kcoreaddons-dev kconfig-dev kconfigwidgets-dev mlt-dev
	kdbusaddons-dev kwidgetsaddons-dev knotifyconfig-dev knewstuff-dev
	knotifications-dev kguiaddons-dev ktextwidgets-dev kiconthemes-dev
	kdoctools-dev kfilemetadata-dev qt5-qtwebkit-dev v4l-utils-dev
	kdeclarative-dev qt5-qtmultimedia-dev rttr-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kdenlive-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="74f007ecdfe2368a1dee9d7b2331e45a56adc157a4d20127659b833e3f045a6edce680593232b9be43e50d16a5222e275af61a4c3a7e4c420ad6721b2e93b502  kdenlive-19.08.1.tar.xz"
