# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksquares
pkgver=19.08.2
pkgrel=0
pkgdesc="Fun and exciting game of Dots and Boxes"
url="https://games.kde.org/game.php?game=ksquares"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfigwidgets-dev
	kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev kwidgetsaddons-dev kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/ksquares-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="fcab0d15f48f0b72d005828ea014ede23c7ceb1949fada90580a9f83f7f6096a240612019080516c67496a99d1a831858c37a829eec864b90f42a5595bb9a711  ksquares-19.08.2.tar.xz"
