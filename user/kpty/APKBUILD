# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpty
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for implementing terminal emulation"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev ki18n-dev libutempter-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpty-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		-DUTEMPTER_EXECUTABLE=/usr/lib/utempter/utempter \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1eb83b3034d950a7561a043f68eea17322f6547a19f399bd500c9ce28bdf02b86d60ed6b4aa976ae4b75cb674ebaa3e4c9c1642601d4470b884ba4ca0ba249c6  kpty-5.54.0.tar.xz"
