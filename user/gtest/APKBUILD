# Maintainer: 
pkgname=gtest
pkgver=1.8.1
pkgrel=0
pkgdesc="C++ testing framework based on xUnit (like JUnit)"
url="https://github.com/google/googletest"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause"
depends="libgcc bash"
depends_dev="python3 cmake"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/google/googletest/archive/release-$pkgver.tar.gz"
builddir="$srcdir"/googletest-release-${pkgver}

build() {
	cd "$builddir"
	rm -rf build
	mkdir build
	cd build

	cmake -DBUILD_SHARED_LIBS=ON \
	      -DCMAKE_SKIP_RPATH=ON ../
	make
}

package() {
	cd "$builddir"
	for dir in usr/lib usr/include/gtest/internal/custom usr/share/licenses/gtest\
			usr/src/gtest/cmake usr/src/gtest/src; do
		install -d -m 0755 "$pkgdir"/"$dir"
	done
	install -m 0644 build/googlemock/gtest/libgtest*.so "$pkgdir"/usr/lib

	install -m 0644 googletest/include/gtest/*.h "$pkgdir"/usr/include/gtest
	install -m 0644 googletest/include/gtest/internal/*.h \
		"$pkgdir"/usr/include/gtest/internal/
	install -m 0644 googletest/include/gtest/internal/custom/*.h \
		"$pkgdir"/usr/include/gtest/internal/custom/
	install -m 0644 googletest/LICENSE "$pkgdir"/usr/share/licenses/$pkgname/
	install -m 0644 googletest/CMakeLists.txt "$pkgdir"/usr/src/gtest/
	install -m 0644 googletest/cmake/* "$pkgdir"/usr/src/gtest/cmake/
}

sha512sums="e6283c667558e1fd6e49fa96e52af0e415a3c8037afe1d28b7ff1ec4c2ef8f49beb70a9327b7fc77eb4052a58c4ccad8b5260ec90e4bceeac7a46ff59c4369d7  release-1.8.1.tar.gz"
