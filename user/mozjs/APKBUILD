# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=mozjs
pkgver=60.5.2
pkgrel=0
pkgdesc="Standalone JavaScript interpreter from Mozilla"
url="https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey"
arch="all"
license="NPL-1.1"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="autoconf2.13 icu-dev libffi-dev nspr-dev python3 cmd:which
	zlib-dev
	ncurses-dev openssl-dev"
subpackages="$pkgname-dev"
source="https://distfiles.adelielinux.org/source/mozilla/mozjs-$pkgver.tar.bz2
	https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tar.xz
	0003-build-Fix-library-install-name-on-macOS.patch
	0009-build-Include-configure-script-be-nicer-about-option.patch
	1002_drop_build_id.patch
	1004_fix_pie_detection.patch
	6006_musl_pthread_setname.patch
	endian.patch
	"
builddir="$srcdir/mozjs-$pkgver"

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-2.7.15"
	[ -d ../python ] && rm -r ../python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install
}

prepare() {
	cd "$builddir"
	default_prepare
	cd "$builddir"/js/src
	export PATH="$srcdir/python/bin:$PATH"
	autoconf-2.13 old-configure.in >/dev/null
	autoconf-2.13
}

build() {
	cd "$builddir"/js/src/build
	export PATH="$srcdir/python/bin:$PATH"
	../configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-jemalloc \
		--disable-optimize \
		--enable-tests \
		--with-intl-api \
		--with-system-icu \
		--with-system-nspr \
		--with-system-zlib
	MOZ_MAKE_FLAGS="$MAKEFLAGS" make
}

check() {
	cd "$builddir"/js/src/build
	dist/bin/jsapi-tests
}

package() {
	cd "$builddir"/js/src/build
	make DESTDIR="$pkgdir" install

	# no real point for 400 MB monster.
	# if someone needs this, we can split it into -dev.
	rm "$pkgdir"/usr/lib/libjs_static.ajs
}

sha512sums="5fb73330e7803bdd524fbe7cfdf4e6b72e85d4b22b0c827400317b5d052d1088d36e558ceac376393089e9d03e658b24e69262851fc04a66bbcda47135423dc0  mozjs-60.5.2.tar.bz2
27ea43eb45fc68f3d2469d5f07636e10801dee11635a430ec8ec922ed790bb426b072da94df885e4dfa1ea8b7a24f2f56dd92f9b0f51e162330f161216bd6de6  Python-2.7.15.tar.xz
8563264274c8b47c41fcce0b23d8d31467c60b4f1e6b37e14a390950f0d4c84be08919e0cf3578367ca1c76633201fc7d182c98a1efb57b17ce176a3e1ed5b0d  0003-build-Fix-library-install-name-on-macOS.patch
2556f3322c5bc39c1efbbbd19b6843cf69b63a0255e8e3617f58b229e75ac221b6cb57fce15452cd1f25498c66f29f588f38c10c175b82c6fe163faaa7e3e2b0  0009-build-Include-configure-script-be-nicer-about-option.patch
0882664005d65d491d4b3d0cd73a2163637730eff41a2ad3ae1ea4e855e8728b83d1bc42632901d717f947a11e6c1c42a33f5286b0adedda3111068ae1425259  1002_drop_build_id.patch
11cef339e8ba5d446d9fbb3947c6526294a3577025f21323cfd959707cbcc36f24535d5b68ea286220891de584ae6deef9408e838fc720af453062a179f22f42  1004_fix_pie_detection.patch
00cc3ea149ea8527393fa3995fcb6fa7af41fc79488c38df2bcd3326dbf43bcc83db509b6dbf2c9c8d68998ce2c1ab1f18c1c46584d9aeb63ddd2cf05b1ca1ce  6006_musl_pthread_setname.patch
363f1df1a78799783b4e9f887dc85d83ec83c6ec392aeacbd98dc89e543d0cd3b9d1964ce274142b44847a03c47f845cbf6fa7fa17a04b25bbaa37fc5d714730  endian.patch"
