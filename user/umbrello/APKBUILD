# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=umbrello
pkgver=19.08.2
pkgrel=0
pkgdesc="Software modelling tool and code generator"
url="https://umbrello.kde.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	qt5-qtwebkit-dev karchive-dev kcompletion-dev kcoreaddons-dev ki18n-dev
	kcrash-dev kdoctools-dev kiconthemes-dev kdelibs4support-dev kio-dev
	ktexteditor-dev kwindowsystem-dev kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/umbrello-$pkgver.tar.xz
	hax.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="a251d6e65cb0b177fee88cadaea1cc9105ac5a0b0b8ab09b31a279c4e473fdeea3acf9a7b54b66178b4d77460bc1406caf4bb03844f8c57f46175bff6c587525  umbrello-19.08.2.tar.xz
fcbe03a49d5420bf17383e5e6d4bac4377c1abfcafacea20ac37409457471537067efe3236647fb3570abfa410a4a3870b1638bb1ef880a24e6f60e2b189a562  hax.patch"
