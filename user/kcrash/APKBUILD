# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcrash
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for gracefully handling software errors~"
url="https://www.kde.org/"
arch="all"
options="!checkroot !check"  # Requires running KDE Plasma 5 session
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev libx11-dev libxext-dev libice-dev kcoreaddons-dev
	kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev"
checkdepends="xkeyboard-config"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcrash-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1dae5de58b8f91e463af8bf745a474472f2d53d2c2ea3bdbff2477c9220a720df1c7f97400e58a3060f891120ced85bb37f9458a7b17b6c1af1be765c9fea671  kcrash-5.54.0.tar.xz"
