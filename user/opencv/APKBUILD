# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer:
pkgname=opencv
pkgver=4.1.1
pkgrel=1
pkgdesc="Computer vision and machine learning software library"
url="https://opencv.org"
arch="all"
options="!check"  # an hour on the talos is madness
license="BSD-3-Clause"
depends=""
makedepends="cmake doxygen ffmpeg-dev gst-plugins-base-dev gtk+2.0-dev
	gtk+3.0-dev jasper-dev libdc1394-dev libgomp libgphoto2-dev
	libjpeg-turbo-dev libpng-dev libwebp-dev tiff-dev v4l-utils-dev"
subpackages="$pkgname-dev $pkgname-libs"
source="opencv-$pkgver.tar.gz::https://github.com/opencv/opencv/archive/$pkgver.tar.gz
	cmake-license.patch
	CVE-2019-16249.patch
	"

# secfixes:
#   4.1.1-r1:
#     - CVE-2019-16249

prepare() {
	default_prepare
	# purge 3rd party except carotene
	for i in 3rdparty/*; do
		case $i in
		*/carotene*) continue;;
		*/protobuf*) continue;;
		*/ittnotify) continue;;  # Else FTBFS on x86_64
		*/quirc)     continue;;
		esac
		rm -rf "$i"
	done
	mkdir -p build
}

build() {
	if [ "$CARCH" != "x86_64" ]; then
		local _sse="-DENABLE_SSE=OFF -DENABLE_SSE2=OFF"
	fi
	if [ "$CARCH" = "ppc" ]; then
		LDFLAGS="-latomic"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -U_FORTIFY_SOURCE" \
		-DCMAKE_C_FLAGS="$CFLAGS -U_FORTIFY_SOURCE" \
		-DENABLE_PRECOMPILED_HEADERS=OFF \
		-DWITH_OPENMP=ON \
		-DWITH_OPENCL=ON \
		-DWITH_OPENEXR=OFF \
		-DWITH_IPP=OFF \
		$_sse \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="80fa48d992ca06a2a4ab6740df6d8c21f4926165486b393969da2c5bbe2f3a0b799fb76dee5e3654e90c743e49bbd2b5b02ad59a4766896bbf4cd5b4e3251e0f  opencv-4.1.1.tar.gz
ffa6930086051c545a44d28b8e428de7faaeecf961cdee6eef007b2b01db7e5897c6f184b1059df9763c1bcd90f88b9ead710dc13b51a608f21d683f55f39bd6  cmake-license.patch
39f2f9abb1051220d6b842e9337c3636ee229781c7efcc92e987dae47ac82072dc95568e6a766e01329ee61c0a3be4efdd82aa3b56c011b44e175444d81c134d  CVE-2019-16249.patch"
