# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sddm
pkgver=0.18.1
pkgrel=2
pkgdesc="Simple Desktop Display Manager"
url="https://github.com/sddm/sddm/"
arch="all"
license="GPL-2.0+"
depends="consolekit2 dbus-x11"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxcb-dev upower-dev
	consolekit2-dev linux-pam-dev qt5-qtdeclarative-dev qt5-qttools-dev
	utmps-dev"
subpackages="$pkgname-lang $pkgname-openrc"
install="sddm.post-install"
langdir="/usr/share/sddm/translations"
pkgusers="sddm"
pkggroups="sddm"
source="https://github.com/sddm/sddm/releases/download/v$pkgver/sddm-$pkgver.tar.xz
	ck2-support.patch
	pam-path-fix.patch
	sddm.initd
	utmpx.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_JOURNALD=OFF \
		-DUID_MIN=500 \
		-DUID_MAX=65000 \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
	install -D -m755 "$srcdir"/sddm.initd "$pkgdir"/etc/init.d/sddm
}

openrc() {
	default_openrc
	depends="sddm xorg-server"
}

sha512sums="ff0637600cda2f4da1f643f047f8ee822bd9651ae4ccbb614b9804175c97360ada7af93e07a7b63832f014ef6e7d1b5380ab2b8959f8024ea520fa5ff17efd60  sddm-0.18.1.tar.xz
075e3baf24606cdea620737dbc00f7cf5c487ab6d88e37b23a581afcfbcf872c379753c89c80ad5c90c28fa27d3e5c5df64d4fd30764fdbfc0586e0f26666fe1  ck2-support.patch
f0b4eb7ef0581701157f9decc637629156f36f6711b9a4bae517f94d7a1df614c81bbd891c918f07ac50e2a3d1519c43ccb9eefd80282c95dd79eca0e8d90904  pam-path-fix.patch
10cac48b821ff7ad39ece4cbc45a8e814d00251b0f8d02f9e42888ad97f465438320f078663be98c5b39630a0bbb26f4f0d76b44574c87a76b4871872add8b9f  sddm.initd
c42d8b3edbc0ae7e3d5ea7bb0080c5c50e0569f0ea947e1ba17bc794c8c0d67a214e62aad7eba0a51791c44b29a3017692bbe738250c63cb2219891bb1313422  utmpx.patch"
