# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgpg
pkgver=19.08.2
pkgrel=0
pkgdesc="Simple interface for GnuPG, a powerful encryption utility"
url="https://utils.kde.org/projects/kgpg/"
arch="all"
options="!check"  # Tests require X11 now.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kcodecs-dev
	kdoctools-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev ki18n-dev
	kiconthemes-dev kjobwidgets-dev kio-dev knotifications-dev kservice-dev
	ktextwidgets-dev kxmlgui-dev kwidgetsaddons-dev kwindowsystem-dev
	akonadi-contacts-dev kcontacts-dev gpgme-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kgpg-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c6dd268fc3e007d4e4fb93317b64606e2b87299352a460f9c2889f61d9828dda93c6c2767e4a1b787fb62fd3b0288eccb466f0e08fb89af6fae845d2bff38654  kgpg-19.08.2.tar.xz"
