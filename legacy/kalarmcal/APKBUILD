# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kalarmcal
pkgver=18.04.3
pkgrel=0
pkgdesc="Calendar library to integrate with your alarm clock"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev kcalcore-dev kidentitymanagement-dev kholidays-dev
	kdelibs4support-dev akonadi-dev kcalutils-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kalarmcal-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d3ff9210cb999d83d108a359e996fcbb187aa0b2bf264331c13f744ac9539a64e678e99b4fac47c19c2edb560c041d07f45450cc3ee56e59c86442755df2c204  kalarmcal-18.04.3.tar.xz"
