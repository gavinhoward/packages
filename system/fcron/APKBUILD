# Contributor: Diaz Devera Victor <vitronic2@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=fcron
pkgver=3.2.1
pkgrel=3
pkgdesc="periodical command scheduler for systems not always up"
url="http://fcron.free.fr/"
pkgusers="fcron"
pkggroups="fcron"
arch="all"
options="suid !check"  # No test suite.
license="GPL"
depends="cmd:sendmail vim"
makedepends="perl"
install="fcron.pre-install"
subpackages="$pkgname-doc $pkgname-openrc"
source="http://fcron.free.fr/archives/${pkgname}-${pkgver}.src.tar.gz
	systab.orig
	fcron.initd"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-editor=/usr/bin/vim \
		--with-spooldir=/var/spool/fcron \
		--sysconfdir=/etc/fcron \
		--with-answer-all=no \
		--with-boot-install=no \
		--localstatedir=/var \
		--with-piddir=/run \
		--with-selinux=no \
		--with-systemdsystemunitdir=no
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir/" install
	install -Dm775 "$srcdir"/fcron.initd "$pkgdir"/etc/init.d/fcron
	rm -r "$pkgdir"/var/run
	install -Dm640 "$srcdir"/systab.orig "$pkgdir"/var/spool/fcron/systab.orig
	install -Dm644 files/fcron.pam "$pkgdir"/etc/pam.d/fcron
	install -Dm644 files/fcrontab.pam "$pkgdir"/etc/pam.d/fcrontab
}

sha512sums="ddfd3e3a297b843c924aacccffaa5c1c6b300497f39daa1cdb90dc4cf4bc757042b0b2c1f055c119c8128c64d830ee0e8757091610432f5ffcacca25d369e5cd  fcron-3.2.1.src.tar.gz
7516c9a51b1f6c29f1256c4697028b47f7883bf5fb08ccc585cf49e4edc7598a9730a225a03aaf3749698a94b9d01dda76cd3f352c0c868960fd09374df44199  systab.orig
8674743ca9080e3cc1e4ebc610dfe362f9d6bd5525447f1f1aa2066a6746d3323ce23ba1fae2e49b222ed0418f9492bbb3a3c73c8239631c5d6ead4b9259630f  fcron.initd"
