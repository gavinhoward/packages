# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libgcrypt
pkgver=1.8.5
pkgrel=0
pkgdesc="GnuPG cryptography library"
url="https://www.gnupg.org"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="libgpg-error-dev texinfo"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.gnupg.org/ftp/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2"

# secfixes:
#   1.8.5-r0:
#     - CVE-2019-13627

build() {
	local _arch_configure=
	case "$CARCH" in
	arm*)
		# disable arm assembly for now as it produces TEXTRELs
		export gcry_cv_gcc_arm_platform_as_ok=no
		;;
	x86 | pmmx | x86_64)
		_arch_configure="--enable-padlock-support"
		;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static \
		$_arch_configure

	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	rm -f ${pkgdir}/usr/share/info/dir
}

sha512sums="b55e16e838d1b1208e7673366971ae7c0f9c1c79e042f41c03d14ed74c5e387fa69ea81d5414ffda3d2b4f82ea5467fe13b00115727e257db22808cf351bde89  libgcrypt-1.8.5.tar.bz2"
