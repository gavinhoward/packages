# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libxslt
pkgver=1.1.33
pkgrel=2
pkgdesc="XML stylesheet transformation library"
url="http://xmlsoft.org/XSLT/"
arch="all"
license="SGI-B-2.0"
makedepends="libxml2-dev libgcrypt-dev libgpg-error-dev python3-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="ftp://xmlsoft.org/$pkgname/$pkgname-$pkgver.tar.gz
	CVE-2019-11068.patch
	CVE-2019-13117.patch
	CVE-2019-13118.patch"

# secfixes:
#   1.1.29-r1:
#     - CVE-2017-5029
#   1.1.33-r1:
#     - CVE-2019-11068
#   1.1.33-r2:
#     - CVE-2019-13117
#     - CVE-2019-13118

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ebbe438a38bf6355950167d3b580edc22baa46a77068c18c42445c1c9c716d42bed3b30c5cd5bec359ab32d03843224dae458e9e32dc61693e7cf4bab23536e0  libxslt-1.1.33.tar.gz
48982b7486351d1eb2853f963db14381dd983c2b4347b7cbeb4507258146ebd8fca125506b2d15d4cbfd2e9ef3fef6341de41a2bfdffc3b0f6bea272b37d9e41  CVE-2019-11068.patch
b311e253a5c4f425f84344397974562a76b253ca14f63b48af7aa0faa561d5f728cb73ee63024993fad3ee7fc7eddb9c9d7310ab8faa5f6a14fd1c6d0037999f  CVE-2019-13117.patch
44d3bb5dda6965f48e3af96c77ffa5f1f2e3c191cf1f28ac1b7b3501420393b5628b12b99fe4008b5056384dfebfdcbbee7625f0644cfc27101424a051415da0  CVE-2019-13118.patch"
