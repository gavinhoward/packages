# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=xz
pkgver=5.2.4
pkgrel=1
pkgdesc="Library and command line tools for XZ and LZMA compressed files"
url="https://tukaani.org/xz/"
arch="all"
license="Public-Domain AND LGPL-2.1+"
depends=""
makedepends=""
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang $pkgname-libs"
source="https://tukaani.org/xz/xz-$pkgver.tar.gz
	dont-use-libdir-for-pkgconfig.patch
	"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/lib \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-rpath \
		--disable-werror

	sed 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
		-i libtool
	sed 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
		-i libtool

	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	make -C "$builddir" DESTDIR="$pkgdir" install
	install -Dm644 "$builddir"/COPYING \
		"$pkgdir"/usr/share/licenses/$pkgname
}

sha512sums="e5bf6eb88365d2dbdc774db49261fb9fae0544ed297891fc20f1ed223f4072cb0357cbd98146ac35b6d29410a12b6739bbd111cd57d4a225bef255ed46988578  xz-5.2.4.tar.gz
9310ae2568dd6ac474e3cb9895e1339ca2dbe8834f856edbb7d2264c0019bde4bbd94aa1edd34e5c8d0aed1f35a1877b0e053ed08a270835ea81e59c7be5edb3  dont-use-libdir-for-pkgconfig.patch"
