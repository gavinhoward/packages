# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=sudo
pkgver=1.8.28
if [ "${pkgver%_*}" != "$pkgver" ]; then
	_realver=${pkgver%_*}${pkgver#*_}
else
	_realver=$pkgver
fi
pkgrel=1
pkgdesc="Give certain users the ability to run some commands as root"
url="https://www.sudo.ws/sudo/"
arch="all"
options="suid"
license="ISC AND MIT AND BSD-3-Clause AND BSD-2-Clause AND Zlib"
depends=""
makedepends_host="linux-pam-dev zlib-dev utmps-dev"
makedepends_build="bash"
makedepends="$makedepends_host $makedepends_build"
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang"
source="https://www.sudo.ws/dist/sudo-${_realver}.tar.gz
	fix-cross-compile.patch
	musl-fix-headers.patch
	SIGUNUSED.patch
	"
builddir="$srcdir"/$pkgname-$_realver

# secfixes:
#   1.8.20_p2-r0:
#     - CVE-2017-1000368
#   1.8.28:
#     - CVE-2019-14287

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libexecdir=/usr/lib \
		--mandir=/usr/share/man \
		--enable-pie \
		--with-env-editor \
		--with-pam \
		--without-skey \
		--with-passprompt="[sudo] Password for %p: " \
		--with-insults=disabled \
		--with-all-insults

	# Workaround until SIGUNUSED.patch is not needed anymore
	rm lib/util/mksiglist.h lib/util/mksigname.h
	make -C lib/util DEVEL=1 mksiglist.h mksigname.h

	make
}

check() {
	make check
}

package() {
	# the sudo's mkinstalldir script miscreates the leading
	# path components with bad permissions. fix this.
	install -d -m0755 "$pkgdir"/var "$pkgdir"/var/db
	make -j1 DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/var/run
}

sha512sums="09e589cdfd18d7c43b0859a0e11c008b3cb995ae4f8c89c717c5242db9e5696361eb574ebe74a0b5316afffb3a8037f7a7f3c249176e8ed9caffeb4cd860ddc7  sudo-1.8.28.tar.gz
f0f462f40502da2194310fe4a72ec1a16ba40f95a821ba9aa6aabaa423d28c4ab26b684afa7fb81c2407cf60de9327bdab01de51b878c5d4de49b0d62645f53c  fix-cross-compile.patch
dcc03abdd672c934f90dfd3683b3f81a8d39cfff91307d2dbd20a31a852022ab605d034c4fe11860ba99b78d391a9812fca1d6e052620b8ff2c42e4f0c7a1a62  musl-fix-headers.patch
2733c220ccbdaf61a32d8c72a5bc0209673733014f0d71b568f1523b71416e9d1754dd8c95bc6cd99aa7f935ed6e93c5f19b1a1dbb7dfc2daf9917fd37f96e78  SIGUNUSED.patch"
