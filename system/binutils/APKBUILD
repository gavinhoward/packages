# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=binutils
pkgver=2.32
pkgrel=5
pkgdesc="Tools necessary to build programs"
url="https://www.gnu.org/software/binutils/"
depends=""
makedepends_build="byacc flex texinfo"
makedepends_host="zlib-dev"
makedepends="$makedepends_build $makedepends_host"
checkdepends="dejagnu"
arch="all"
license="GPL-2.0+ AND GPL-3.0+ AND LGPL-2.0+ AND LGPL-2.1+ AND LGPL-3.0+ AND BSD-3-Clause"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
# non-PIC is unsupported by musl/ppc
[ "${CARCH}" != "ppc" ] || options='!check'
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	arm-pie.patch
	binutils-ld-fix-static-linking.patch
	disable-gnu-mbind.patch
	disable-ifunc-tests.patch
	disable-preinit-array-tests.patch
	remove-pr2404-tests.patch
	remove-pr19719-test.patch
	remove-pr19553c-test.patch
	srec.patch
	CVE-2019-9070-and-9071.patch
	CVE-2019-9073.patch
	CVE-2019-9074.patch
	CVE-2019-9075.patch
	CVE-2019-9077.patch
	CVE-2019-12972.patch
	CVE-2019-14250.patch
	CVE-2019-14444.patch
	CVE-2019-17450.patch
	CVE-2019-17451.patch
	BTS-170.patch
	BTS-196.patch
	"

if [ "$CHOST" != "$CTARGET" ]; then
	pkgname="$pkgname-$CTARGET_ARCH"
	subpackages=""
	options="!check"
	sonameprefix="$pkgname:"
	builddir="$srcdir"/binutils-$pkgver
fi

# secfixes: binutils
#   2.28-r1:
#     - CVE-2017-7614
#   2.31.1-r2:
#     - CVE-2018-19931
#     - CVE-2018-19932
#   2.32-r0:
#     - CVE-2018-1000876
#   2.32-r2:
#     - CVE-2019-9070
#     - CVE-2019-9071
#     - CVE-2019-9073
#     - CVE-2019-9074
#     - CVE-2019-9075
#     - CVE-2019-9077
#     - CVE-2019-12972
#     - CVE-2019-14250
#   2.32-r3:
#     - CVE-2019-14444
#   2.32-r5:
#     - CVE-2019-17450
#     - CVE-2019-17451

build() {
	local _sysroot=/
	local _cross_configure="--enable-install-libiberty"
	local _arch_configure=""

	if [ "$CHOST" != "$CTARGET" ]; then
		_sysroot="$CBUILDROOT"
		_cross_configure="--disable-install-libiberty"
	fi

	# -z separate-code is currently causing kernel miscompiles on x86*
	case "$CTARGET_ARCH" in
	x86 | pmmx | i528) _arch_configure="--disable-separate-code" ;;
	x86_64) _arch_configure="--disable-separate-code --enable-targets=x86_64-pep" ;;
	esac

	case "$CTARGET_ARCH" in
	mips*)  _hash_style_configure="--enable-default-hash-style=sysv" ;;
	*)      _hash_style_configure="--enable-default-hash-style=gnu"	;;
	esac

	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--with-build-sysroot="$CBUILDROOT" \
		--with-sysroot=$_sysroot \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-multilib \
		--enable-shared \
		--enable-ld=default \
		--enable-64-bit-bfd \
		--enable-plugins \
		--enable-relro \
		--disable-gold \
		--enable-deterministic-archives \
		$_cross_configure \
		$_arch_configure \
		$_hash_style_configure \
		--disable-werror \
		--with-system-zlib \
		--enable-lto
	make
}

package() {
	cd "$builddir"
	make install DESTDIR="$pkgdir"
	if [ -d "$pkgdir"/usr/lib64 ]; then
		die "Something really bad happened."
		mv "$pkgdir"/usr/lib64/* "$pkgdir"/usr/lib/
		rmdir "$pkgdir"/usr/lib64
	fi
	if [ "$CHOST" != "$CTARGET" ]; then
		# creating cross tools: remove any files that would conflict
		# with the native tools, or other cross tools
		rm -r "$pkgdir"/usr/share
		rm -f "$pkgdir"/usr/lib/libiberty.a
	fi
}

check() {
	cd "$builddir"
	make check
}

libs() {
	pkgdesc="Runtime libraries from binutils"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so "$subpkgdir"/usr/lib/
}

sha512sums="d326408f12a03d9a61a9de56584c2af12f81c2e50d2d7e835d51565df8314df01575724afa1e43bd0db45cfc9916b41519b67dfce03232aa4978704492a6994a  binutils-2.32.tar.xz
2f862c473b357effe63221a47c4a34893eff50e4ab3c7b500b19754f3c7bf81ee695c35e5ce16f6c6fcd75696d270d04f282ae4c7d32b42a61b667ff1eccce8f  arm-pie.patch
ecee33b0e435aa704af1c334e560f201638ff79e199aa11ed78a72f7c9b46f85fbb227af5748e735fd681d1965fcc42ac81b0c8824e540430ce0c706c81e8b49  binutils-ld-fix-static-linking.patch
d378fdf1964f8f2bd0b1e62827ac5884bdf943aa435ec89c29fc84bb045d406b733fffaff8fdd8bd1cba8ddea7701c4cf6ccf3ed76a8a3df9c72b447737575a6  disable-gnu-mbind.patch
474ab24097bbb5b24433620549e5234fe65c547824c1342f693c718ffbc81e2d968259cce2d650b55200dd1ec89da207ea2db10c551cd9941285c4600b4297b2  disable-ifunc-tests.patch
3537752e63cef0b5ef136d003ff7e814ba66b12624d817430112d0f291a792e8960fa69a78036f526af835441b3ee483d6a53d55c7b3dd8ee96f0399682dbcbe  disable-preinit-array-tests.patch
32ab4215669c728648179c124632467573a3d4675e79f0f0d221c22eb2ec1ca5488b79910bd09142f90a1e0d0b81d99ca4846297f4f9561f158db63745facb66  remove-pr2404-tests.patch
a193d1fa7f42d91915960460a15e4d24e0df529d81e23014bcf45d283fae76bb7b300fdcb0d0a9d521cdb9137322efa1dc357112596d6ae7a7fd05988ac359b9  remove-pr19719-test.patch
39ef9c76dd5db6b15f11ffa8061f7ca844fb79c3fb9879c3b1466eef332a28b833597c87003ab9f260b1b85023fae264659088aee27cad7e5aa77b2d58b9a3f6  remove-pr19553c-test.patch
f720b3356b88e366c52941da056e543e4b42bc77f012e5b0290f79e15b0a31d855989ad01920680507a9df0544e5b8e26d0cf8d6f22fbdeb874af31cff4c16d3  srec.patch
f52d21f194c2d7dbdc56e93636d3228034ee1718b457e5a5ce289bba2454155846d1ff6ea8530d11a901a85c9af945360bc17cda9e7370c36362aa6c762154c7  CVE-2019-9070-and-9071.patch
032fed723b610fe06e210e2ebee8d24962ecad1dc69d98d38e95f768c9ed64cb991158758ef71e684d6d762a30e9a852287836be2bb8a2aba27fe31d2792c0a0  CVE-2019-9073.patch
16b4cc094a6846399e47271da6fe8d8bd8b70246e12e872fcafb85f11809b5699eddba723fbac664c062c02f9b5658ea9770e14c522e151cdea1d39e69c851dd  CVE-2019-9074.patch
a46b9211608e2f35219b95363a5ba90506742dcb9e4bd4a43915af6c0b3e74bd8339a8318dc2923c0952ef579112412cb1cf619a5f090066769a852587b27d03  CVE-2019-9075.patch
c0f50f1a843480f29b3895c8814df9801b9f90260edbaff1831aa5738fedd07a9e6b7a79f5b6f9be34df4954dbf02feb5232ebbecc596277fc2fe63673ed347c  CVE-2019-9077.patch
9109a6ff9c55f310f86a1561fe6b404534928d402672490059bbe358f77c0c2a7f73c8b67f0a4450f00ba1776452858b63fa60cf2ec0744104a6b077e8fa3e42  CVE-2019-12972.patch
c277202272d9883741c2530a94c6d50d55dd9d0a9efaa43a1f8c9fc7529bd45e635255c0d90035dfc5920d5387010a4259612a4d711260a95d7b3d9fa6500e4f  CVE-2019-14250.patch
0942cc1a4c5ec03e931c6ebd15c5d60eae6be48cd0a3d9b7f6356f97361226bb6d53dbdcb01b20efcca0ccaf23764730d9bbad2c1bbe2ea6ca320e43b43b311b  CVE-2019-14444.patch
4e8cbe3985ca4a7cb8954e4e03f094687985b3afec6bb14f1599665e0ab13e601b68cefdbb63e88f9dd59852036dcfee05af14014493c16c76dc38d406efc8fd  CVE-2019-17450.patch
a71a035db5e14f105b5d58ec01ad250447f6282cae04e8c931fbdbf7adf118a065c6c9be9c72a204e0f8115b19598dc52f3953ae91200d48328b58cc274939d8  CVE-2019-17451.patch
d4543d2f77808d317d17a5f0eb9af21540ef8543fceaed4e3524213e31e058333321f3ba3b495199e3b57bfd0c4164929cf679369470389e26871b8895cb0110  BTS-170.patch
9cc17d9fe3fc1351d1f6b4fc1c916254529f3304c95db6f4698b867eeb623210b914dc798fb837eafbad2b287b78b31c4ed5482b3151a2992864da04e1dd5fac  BTS-196.patch"
