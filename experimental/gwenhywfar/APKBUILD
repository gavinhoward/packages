# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gwenhywfar
pkgver=4.20.0
pkgrel=0
pkgdesc="Multi-purpose financial library"
url="https://www.aquamaniac.de/rdm/projects/gwenhywfar"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev gnutls-dev libgcrypt-dev libgpg-error-dev
	cmd:which gtk+2.0-dev"
subpackages="$pkgname-dev $pkgname-gtk2 $pkgname-qt5"
source="gwenhywfar-$pkgver.tar.gz::https://www.aquamaniac.de/sites/download/download.php?package=01&release=208&file=02&dummy=gwenhywfar-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-system-certs \
		--disable-network-checks \
		--with-guis="qt5 gtk2" \
		aq_distrib_name="Adélie" \
		aq_distrib_tag="adelie"
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

gtk2() {
	pkgdesc="$pkgdesc (Gtk+ 2.0 UI)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*gtk2* "$subpkgdir"/usr/lib/
}

qt5() {
	pkgdesc="$pkgdesc (Qt 5 UI)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*qt5* "$subpkgdir"/usr/lib/
}

sha512sums="bc1d47c39654940198396f1bdb9f4a87ff4e0e6b07cb4525c10f7f8e6ce93445d5b9d85586a47c93c558e86eb826f7006eca29b93fad6371dfe72830bf9b88e1  gwenhywfar-4.20.0.tar.gz"
